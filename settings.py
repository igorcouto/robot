import os
import glob

project_name = 'robot'

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
SRC_DIR = os.path.join(ROOT_DIR, project_name)
DATA_DIR = os.path.join(ROOT_DIR, 'data')
PROXIES_DATA = os.path.join(DATA_DIR, 'proxies.csv')
CUSTOMERS_DATA = os.path.join(DATA_DIR, 'customers.csv')
PRODUCTS_DATA = os.path.join(DATA_DIR, 'products.csv')
TESTS_DIR = os.path.join(SRC_DIR, 'tests')
LOG_DIR = os.path.join(ROOT_DIR, 'logs')
PROXIES_LOG = os.path.join(LOG_DIR, 'proxies.log')
COMPRESS_USERDATA_LOG = os.path.join(LOG_DIR, 'compress_userdata.log')
USERDATA_TEMP = os.path.abspath(r'D:\tmp')
USERDATA_ZIPPED = os.path.abspath(r'D:\zipped')
PROXY_REFRESH_TIME = 15
HIGH_PROXY_REFRESH_TIME = 120 # Proxies that releases non-USA IPs are fewer frequently updated
CHROMEDRIVER_DIR = os.path.join(ROOT_DIR, 'chromedriver')
CHROMEDRIVER_EXE = os.path.join(CHROMEDRIVER_DIR, 'chromedriver.exe')
DRIVER_SETTINGS = {'--lang': 'en',
                   '--log-level': 3,
                   'disable-automation-extension': True,
                   'password-manager': True,
                   'extensions': [f for f in glob.glob(r'%s\*.crx' % CHROMEDRIVER_DIR)]}
MONITOR_RESOLUTIONS = ['1366,768', '1920,1080', '1536,864', '1440,900', '1280,720', '1600,900',
                       '1280,720', '1280,800', '1280,1024', '1024,768']
if __name__ == '__main__':
    # Prints all settings variables of the project.
    print('\nAll settings of %s project.' % project_name.upper())
    variables = [v for v in dir() if not v.startswith("__") and v.isupper()]
    for v in variables:
        value = globals()[v]
        print('- %s::: %s' % (v, value) )