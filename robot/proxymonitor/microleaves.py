# Built-in modules
import json
import time
import logging
import logging.handlers
from threading import Thread
from datetime import datetime as dt
from multiprocessing import Process, Manager
# Third-party modules
import inflect
import tzlocal
import requests
# Project modules
import settings


def configure_logger(log_queue, log_file):
    """Configures a logger based in a multiprocessing.Queue. It is necessary
    because of multiprocesing and logging "incompatibility". Each module
    (process) need to have your own configure_logger method.

    Arguments:
        log_queue {multiprocessing.Queue} -- An object (FIFO) to store the log
        log_file {string} -- The path to store additional log messages.
            messages.
    """
    # Handler to log in the prompt
    h = logging.handlers.QueueHandler(log_queue)
    h.setLevel(logging.INFO)

    # Handler to log in a file
    f = logging.FileHandler(log_file,  mode='a')
    formatter = logging.Formatter(
        '%(asctime)s | %(threadName)s | %(levelname)s | %(message)s',
        datefmt='%m/%d/%y %H:%M:%S'
    )
    f.setFormatter(formatter)
    f.setLevel(logging.DEBUG)

    root = logging.getLogger()
    root.addHandler(h)
    root.addHandler(f)

    # Set level of logger
    root.setLevel(logging.INFO)

def request_to_ip_api(proxy_host, ip_api='http://ip-api.com/json'):
    """Requests a page to gather proxy information.

    Arguments:
        proxy_host {str} -- the proxy host like <IP>:<PORT>

    Keyword Arguments:
        ip_api {str} -- the IP-API site to retrieve (default: {'http://ip-api.com/json'})

    Returns:
        dict -- a dict with information requested.
    """
    # Creates a proxy dictionary to store
    proxy = {}

    # Creates the proxy object
    if proxy_host:
        http_proxy = {'http': 'http://%s' % proxy_host}
    else:
        http_proxy = None

    # Calculates latency
    start_time = time.time()
    r = requests.get(ip_api, proxies=http_proxy)
    latency = time.time() - start_time

    # Updates if request have response
    if r.status_code == 200:
        proxy.update(r.json())
    else:
        proxy.update({'status': r.content.decode().strip()})

    # Adds latency keys in proxy
    proxy.update({'latency': latency})

    return proxy

def get_proxy_info(proxy_host):
    """Gets proxy info and handles some errors.

    Arguments:
        proxy_host {str} -- proxy_host {str} -- the proxy host like <IP>:<PORT>
    """
    try:
        proxy_info = request_to_ip_api(proxy_host=proxy_host)
    except json.JSONDecodeError as e:
        proxy_info = {'host': proxy_host,
                      'status': e.msg}
    except Exception as e:
        proxy_info = {'host': proxy_host,
                      'status': str(e)}

    return proxy_info

def log_info_message(proxy):
    """Creates a log message
    """
    proxy_info = proxy.get('info')
    msg = '{} | {} | {}/{}/{} | Releases:: {} | Available:: {}'.format(
        proxy_info.get('query'),
        proxy_info.get('status'),
        proxy_info.get('city'),
        proxy_info.get('regionName'),
        proxy_info.get('countryCode'),
        proxy.get('counter'),
        proxy.get('available').is_set()
    )
    return msg

def update_proxy(m_proxy, logger):
    """Updates a multiprocessing.Manager().dict() object periodically.

    Arguments:
        m_proxy {multiprocessing.Manager()} -- a shareable dictionary between processes.
        logger {Queue} -- A logger to send log messages.
    """
    # Freezes a copy to compare
    old_m_proxy = dict(m_proxy.copy())

    # Retrieves proxy info
    m_proxy.update({'info': get_proxy_info(proxy_host=m_proxy.get('proxy'))})

    # Conditions to update proxy
    is_success = m_proxy.get('info').get('status') == 'success' # Is the last request a success?
    is_new_ip = old_m_proxy.get('info').get('query') != m_proxy.get('info').get('query') # Did proxy change the IP?
    n_releases = old_m_proxy.get('counter')
    is_USA_IP = m_proxy.get('info').get('countryCode') == 'US'

    if is_success:
        # Only IP's from USA
        if not is_USA_IP:
            # Turns proxy unavailable to other processes
            m_proxy.get('available').clear()
            m_proxy.update({'refresh': settings.HIGH_PROXY_REFRESH_TIME})
            if is_new_ip:
                logger.warn('Non-USA IP in %s :: %s' % (m_proxy.get('proxy'),
                                                        m_proxy.get('info').get('country')))
            return

        # Keep refresh time normal
        m_proxy.update({'refresh': settings.PROXY_REFRESH_TIME})

        if is_new_ip:
            # Updates timestamp and counter keys
            m_proxy.update({'last_update': dt.now(tz=tzlocal.get_localzone()),
                            'counter': n_releases + 1})

            if n_releases > 0:
                # Turns proxy available to other processes
                m_proxy.get('available').set()
                # Notifies another processes that IP changed!
                m_proxy.get('change_ip').set()
                logger.info(log_info_message(m_proxy))

    else:
        # Turns proxy unavailable to other processes
        m_proxy.get('available').clear()

    # Clears change ip event
    if m_proxy.get('change_ip').is_set():
        m_proxy.get('change_ip').clear()

def run_proxy_thread(m_proxy, logger):
    """Runs indefinitely

    Arguments:
        m_proxy {multiprocessing.Manager()} -- a shareable dictionary between processes.
        logger {Queue} -- A logger to send log messages.
    """
    logger.info('Monitoring %s...' % m_proxy.get('proxy'))
    while True:
        update_proxy(m_proxy=m_proxy, logger=logger)
        time.sleep(m_proxy.get('refresh'))

class ProxyMonitorProcess(Process):
    """This class provides a proxy to monitors a group of proxies.
    """
    def __init__(self, m_proxies, log_queue):
        """Overrides Process __init__ method.

        Arguments:
            m_proxies {Manager} -- An object shared between processes
            log_queue {Queue} -- A multiprocessing.Queue object to send log messages
            log_file {string} -- The path to store additional log messages.
        """
        # Calls Process __init__ method
        Process.__init__(self, name='ProxyMonitor')
        self.m_proxies = m_proxies
        self.proxy_threads = []
        M = Manager()
        self.available_proxies = M.Event()
        self.at_least_a_proxy_success = M.Event()
        self.log_queue = log_queue
        self.log_file = settings.PROXIES_LOG

    def create_threads(self, logger):
        """Creates a pool of threads to monitor proxies. Each thread handles a proxy.

        Arguments:
            logger {logging.getLogger} -- A logger to send log messages.

        Returns:
            list -- a list of threading.Thread objects.
        """
        threads = []
        for proxy in self.m_proxies:
            t = Thread(target=run_proxy_thread,
                       args=(proxy, logger),
                       name=proxy.get('proxy'))
            threads.append(t)

        return threads

    def get_available_proxies(self):
        """Gets all available proxies
        """
        proxies = []
        for p in self.m_proxies:
            # Check is available event in proxy is set
            if p.get('available').is_set():
                proxies.append(p)

        return proxies

    def get_successful_proxies(self):
        proxies = []
        for p in self.m_proxies:
            p_info = p.get('info')
            if p_info:
                if p_info.get('status') == 'success':
                    proxies.append(p)

        if len(proxies) > 0:
            self.at_least_a_proxy_success.set()
        else:
            self.at_least_a_proxy_success.clear()

        return proxies

    def run(self):
        """Overrides Process run method.
        """
        # Configures logging module and creates a logger
        configure_logger(log_queue=self.log_queue, log_file=self.log_file)
        logger = logging.getLogger('ProxyMonitor')
        logger.info('Started.')

        threads = self.create_threads(logger=logger)
        # Starts threads
        [t.start() for t in threads]

        # Another processes can be waiting for the first proxy release
        waiting_the_first_release = True

        # While threads run, updates available_proxies event.
        while True:
            if not self.at_least_a_proxy_success.is_set():
                successul_proxies = self.get_successful_proxies()

            available_proxies = self.get_available_proxies()
            if len(available_proxies) > 0:
                self.available_proxies.set()
                # Avoid multiples log messages in a short period of time
                if waiting_the_first_release:
                    pl = inflect.engine()
                    num = len(available_proxies)
                    logger.info('%s %s available. Signal sent to the MainProcess.' % (
                        num,
                        pl.plural('proxy', num))
                    )
                    waiting_the_first_release = False
            else:
                self.available_proxies.clear()

            time.sleep(10)

        # Waits for threads
        [t.join() for t in threads]

        logger.info('Finished.')
