# Built-in modules
import os
import random
import logging
import logging.handlers
# Third-party modules
from selenium import webdriver
from fake_useragent import UserAgent
# Project modules
import settings


def configure_logger(log_queue):
    """Configures a logger based in a multiprocessing.Queue. It is necessary
    because of multiprocesing and logging "incompatibility". Each module
    (process) need to have your own configure_logger method.

    Arguments:
        log_queue {multiprocessing.Queue} -- An object (FIFO) to store the log
    """
    # Handler to log in the prompt
    h = logging.handlers.QueueHandler(log_queue)
    h.setLevel(logging.INFO)

    root = logging.getLogger()
    root.addHandler(h)

    # Set level of logger
    root.setLevel(logging.INFO)

def create_options(logger, proxy):
    """Creates random Chrome options

    Returns:
        [type] -- [description]
    """
    # Creating random User-Agent and resolution parameters
    user_agent = UserAgent().random
    resolutions = ['1366,768', '1920,1080', '1536,864', '1440,900', '1280,720',
                   '1600,900', '1024,768', '1280,1024', '1280,800']
    resolution = random.choice(resolutions)

    logger.info('User-Agent:: %s...' % user_agent[:50])
    logger.info('Resolution:: %s.' % resolution)

    # Instanciates an option to populate
    opt = webdriver.ChromeOptions()
    opt.add_argument('user-agent=%s' % user_agent)
    opt.add_argument('window-size=%s' % resolution)
    opt.add_argument('--lang=%s' % 'en-Us')

    # Removing "Chrome is being controlled..."
    opt.add_experimental_option("excludeSwitches", ['enable-automation'])
    opt.add_experimental_option('useAutomationExtension', False)

    # Avoid to chrome shows a save password pop-up
    opt.add_experimental_option('prefs', {
        'credentials_enable_service': False,
        'profile': {
            'password_manager_enabled': False
        }
    })
    # Searching for extensions on chromedriver folder and loading them
    crx_files = []
    for r, d, f in os.walk(settings.CHROMEDRIVER_DIR):
        for file in f:
            if '.crx' in file: crx_files.append(os.path.join(r, file))
    # Loading
    for f in crx_files:
        opt.add_extension(f)

    # Adding on ChromeOption object
    opt.add_argument('--proxy-server=%s' % proxy.get('proxy'))
    logger.info('Proxy:: %s.' % proxy.get('proxy'))

    # Silent mode
    opt.add_argument("--log-level=3")

    return opt

def execute_iteraction(log_queue, proxy):
    # Configures logging module and creates a logger
    configure_logger(log_queue=log_queue)
    logger = logging.getLogger('Add to Cart')
    logger.info('Started.')

    opt = create_options(logger, proxy)
    driver = webdriver.Chrome(executable_path=settings.CHROMEDRIVER_EXE, options=opt)
    driver.get('https://amazon.com')
    import time
    time.sleep(10)
    driver.quit()
