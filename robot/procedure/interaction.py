# Built-in modules
import random
import logging
import logging.handlers
from multiprocessing import Process, Queue, Manager
# Third-party module
import psutil
# Project modules
import settings
from robot.driver import chromedriver
from robot.actions import atc


def configure_logger(log_queue):
    """Configures a logger based in a multiprocessing.Queue. It is necessary
    because of multiprocesing and logging "incompatibility". Each module
    (process) need to have your own configure_logger method.

    Arguments:
        log_queue {multiprocessing.Queue} -- An object (FIFO) to store the log
    """
    # Handler to log in the prompt
    h = logging.handlers.QueueHandler(log_queue)
    h.setLevel(logging.INFO)

    root = logging.getLogger()
    root.addHandler(h)

    # Set level of logger
    root.setLevel(logging.INFO)

class Interaction(Process):
    driver = None
    def __init__(self, action, **kwargs):
        """Overrides Process init method
        """
        # Queue used to log messages to console
        if kwargs.get('log_queue'):
            self.log_queue = kwargs.get('log_queue')
        else:
            self.log_queue = Queue(-1)
        # Product
        if kwargs.get('product'):
            self.product = kwargs.get('product')
        # Driver settings
        self.driver_settings = kwargs.get('driver_settings')
        # Proxies
        self.proxy = kwargs.get('proxy')
        if self.proxy:
            if self.driver_settings:
                self.driver_settings.update({'--proxy-server': self.proxy.get('proxy')})
            else:
                self.driver_settings = {'--proxy-server': self.proxy.get('proxy')}

        # Driver instance
        self.driver = chromedriver.ChromeDriver(executable_path=settings.CHROMEDRIVER_EXE,
                                                browser_settings=self.driver_settings)
        self.driver_is_available = self.driver.is_available
        M = Manager()
        self.driver_pid = M.Value(int, 0)
        # Action or actions
        self.action = action
        # Calls init Process method
        Process.__init__(self, name=kwargs.get('name'))

    def run(self):
        """Overrides Process run method.
        """
        configure_logger(log_queue=self.log_queue)
        logger = logging.getLogger('Interaction')
        logger.info('Started.')
        p = self.proxy.get('proxy')
        pi = self.proxy.get('info')
        logger.info('Proxy:: %s - %s (%s/%s)' % (p, pi.get('query'), pi.get('city'), pi.get('regionName')))
        logger.info('User-Agent:: %s' % self.driver_settings.get('user-agent')[0:50])
        logger.info('Resolution:: %s' % self.driver_settings.get('window-size'))
        logger.info('Keywords:: %s' % self.product.get('keywords'))
        logger.info('ASINS:: %s' % self.product.get('asins'))

        # Driver instance lives only here. Take care to avoid
        # access driver outside this scope.
        self.driver.initialize()
        self.driver_pid.value = self.driver.pid

        keyword = random.choice(self.product.get('keywords'))
        asins = self.product.get('asins')
        # Runs actions
        self.action(self.driver.driver, keyword, asins)

        # Closes browser
        self.driver.quit()
        logger.info('Finished gracefully.')

    def terminate(self):
        """Overrides Process terminate method.
        """
        # Closes driver process if is alive
        pid = self.driver_pid.value
        if psutil.pid_exists(pid):
            process = psutil.Process(pid)
            children = process.children()
            # Kills the children of driver instance.
            try:
                [psutil.Process(p.pid).kill() for p in children]
            except (PermissionError, psutil.AccessDenied):
                pass
            try:
                psutil.Process(pid).kill()
            except (PermissionError, psutil.AccessDenied):
                pass
            try:
                # Kills the chromedriver process itself
                psutil.wait_procs([process])
            except (PermissionError, psutil.AccessDenied):
                pass

        if self.driver_is_available.is_set():
            self.driver_is_available.clear()

        # Calls super terminates method
        Process.terminate(self)
