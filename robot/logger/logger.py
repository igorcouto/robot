import logging


def configure_logger():
    """Configures a root logger.
    """
    root = logging.getLogger()
    h = logging.StreamHandler()
    f = logging.Formatter(
        '%(asctime)s | %(processName)s | %(levelname)s | %(message)s',
        datefmt='%m/%d/%y %H:%M:%S'
    )
    h.setFormatter(f)
    root.addHandler(h)

def process(log_queue):
    """Reads the queue and send the message to logger.
    Arguments:
        queue {multiprocesing.Queue} -- A Queue which messages are stored.
    """
    configure_logger()
    while True:
        record = log_queue.get()
        # We send this as a sentinel to tell the listener to quit
        if record is None:
            break
        logger = logging.getLogger(record.name)
        logger.handle(record)