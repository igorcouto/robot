# Built-in module
import random
import logging
# Third-party modules
from fake_useragent import UserAgent
# Project modules
import settings
from robot.data import loader
from robot.driver import chromedriver
from robot.procedure import interaction
from robot.proxymonitor import microleaves
from robot.actions import atc


def create_driver_settings():
    driver_settings = settings.DRIVER_SETTINGS
    ua = UserAgent()
    driver_settings.update({'user-agent': ua.random})
    driver_settings.update({'window-size': random.choice(settings.MONITOR_RESOLUTIONS)})
    return driver_settings

def run(log_queue):
    """Runs ATC (Add to cart) procedure.
    """
    # Loads proxies and customers
    PROXIES, _ = loader.load_CSV_to_managers()
    PRODUCTS = loader.load_products()

    # Create and starts a ProxyMonitorProcess
    pm_process = microleaves.ProxyMonitorProcess(m_proxies=PROXIES, log_queue=log_queue)
    pm_process.start()

    # Waiting for a new proxy release
    pm_process.at_least_a_proxy_success.wait()

    i = 0
    while True:
        proxies = pm_process.get_successful_proxies()
        proxy = random.choice(proxies)
        driver_settings = create_driver_settings()
        product = random.choice(PRODUCTS)
        inter = interaction.Interaction(action=atc.run,
                                        log_queue=log_queue,
                                        name='Interaction-%s' % i,
                                        proxy=proxy,
                                        driver_settings=driver_settings,
                                        product=product)
        inter.start()

        # Runs until proxy doenst change ip.
        while inter.is_alive():
            if proxy.get('change_ip').is_set():
                inter.terminate()
                logging.warn('Interaction-%s terminated by change in IP.' % i)

        inter.join()
        i += 1

    # Waits all processes to finish
    pm_process.terminate()