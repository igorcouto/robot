# Built-in module
import os
import time
import random
import logging
# Third-party modules
from selenium import webdriver
from fake_useragent import UserAgent
# Project modules
import settings
from robot.actions import user_actions


logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s | %(processName)s | %(levelname)s | %(message)s',
                    datefmt='%m/%d/%y %H:%M:%S')

def create_options():
    """Creates random Chrome options

    Returns:
        [type] -- [description]
    """
    # Creating random User-Agent and resolution parameters
    user_agent = UserAgent().random
    resolutions = ['1366,768', '1920,1080', '1536,864', '1440,900', '1280,720',
                   '1600,900', '1024,768', '1280,1024', '1280,800']
    resolution = random.choice(resolutions)

    logging.info('User-Agent:: %s...' % user_agent[:50])
    logging.info('Resolution:: %s.' % resolution)

    # Instanciates an option to populate
    opt = webdriver.ChromeOptions()
    opt.add_argument('user-agent=%s' % user_agent)
    opt.add_argument('window-size=%s' % resolution)
    opt.add_argument('--lang=%s' % 'en-Us')

    # Removing "Chrome is being controlled..."
    opt.add_experimental_option("excludeSwitches", ['enable-automation'])
    opt.add_experimental_option('useAutomationExtension', False)

    # Avoid to chrome shows a save password pop-up
    opt.add_experimental_option('prefs', {
        'credentials_enable_service': False,
        'profile': {
            'password_manager_enabled': False
        }
    })
    # Searching for extensions on chromedriver folder and loading them
    crx_files = []
    for r, d, f in os.walk(settings.CHROMEDRIVER_DIR):
        for file in f:
            if '.crx' in file: crx_files.append(os.path.join(r, file))
    # Loading
    for f in crx_files:
        opt.add_extension(f)

    # Silent mode
    opt.add_argument("--log-level=3")

    return opt

def run():
    logging.info('Started.')

    # Creates options and driver
    opt = create_options()
    driver = webdriver.Chrome(executable_path=settings.CHROMEDRIVER_EXE, options=opt)

    # Runs user actions
    logging.info('Clicking in nav-logo.')
    user_actions.click_on_nav_logo(driver)

    # Click in several links
    for i in range(5):
        link = user_actions.click_in_some_link(driver)
        logging.info('%s clicked.' % link.get('text'))

    logging.info('Going to main page.')
    user_actions.go_to_main_page(driver)

    user_actions.search_product(driver)

    time.sleep(30)

    driver.get('https://amazon.com')