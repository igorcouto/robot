# Built-in libraries
import csv
from multiprocessing import Manager
# Project libraries
import settings


def split_and_strip(string, delimiter):
    """Splits and strips a string by a delimiter."""
    return [k.strip() for k in string.split(delimiter)]

def read_csv(file, delimiter=';', quotechar='"'):
    """Reads an CSV file and saves in a list of dictionaries. Avoid to use outside
    this module.

    Arguments:
        file {str} -- The CSV file path to read

    Keyword Arguments:
        delimiter {str} -- The delimiter of the CSV file (default: {';'})
        quotechar {str} -- The quote character of the CSV file (default: {'"'})

    Returns:
        list -- The list of dictionaries
    """
    data = []

    with open(file) as f:
        reader = csv.DictReader(f, delimiter=delimiter, quotechar=quotechar)
        for r in reader:
            data.append(dict(r))

    return data

def load_proxies(proxies):
    """Loads proxies to a multiprocessing.Manager list

    Arguments:
        file {str} -- The CSV file path to read

    Returns:
        multiprocessing.Manager -- An object (list) that can be shared between process.
    """
    # Creates a multiprocessing.Manager list to store data
    m = Manager()
    m_list = m.list()

    for p in proxies:
        p.update({'available': m.Event()})
        p.update({'change_ip': m.Event()})
        p.update({'counter': 0})
        p.update({'info': {}})
        p.update({'refresh': settings.PROXY_REFRESH_TIME})
        m_list.append(m.dict(p))

    return m_list

def load_customers(customers):
    """Loads customers to a multiprocessing.Manager list

    Arguments:
        file {str} -- The CSV file path to read

    Returns:
        multiprocessing.Manager -- An object (list) that can be shared between process.
    """
    # A manager stores an object that can be shared between processes.
    m = Manager()
    m_list = m.list()

    for c in customers:
        # Pre-processing
        # Adds the imap_host key
        if not c.get('imap_host'):
            c.update({'imap_host': 'imap.mail.yahoo.com'})
        # Adds status key
        c.update({'status': 'unknow'})
        # If selected appends to manager
        if c.get('select'):
            m_list.append(m.dict(c))

    return m_list

def load_products(file=settings.PRODUCTS_DATA):
    """Loads products data inside a list - multiprocessing.Manager().list.
    Here are placed filters and pre-processing tasks of input data.

    Keyword Arguments:
        file {str} -- The CSV file path to read. (default: {settings.PRODUCTS_DATA})

    Returns:
        multiprocessing.Manager -- An list to be shared between several process.
    """
    m = Manager()
    prod_list = m.list()
    # Reads data
    prod_data = read_csv(file=file)
    for p_data in prod_data:
        # Only selected
        if p_data.get('select'):
            p = m.dict(p_data) # shareable object
            # Transforms some keys in list
            for k in ['asins', 'weights', 'keywords']:
                if p.get(k):
                    p.update({k: split_and_strip(p.get(k), ',')})

            prod_list.append(p)

    return prod_list

def load_CSV_to_managers():
    """Loads CSV files (proxies and customers) to managers.

    Returns:
        list -- A list with two objects that can be shared between processes.
    """
    p_data = read_csv(file=settings.PROXIES_DATA)
    m_proxies = load_proxies(proxies=p_data)
    c_data = read_csv(file=settings.CUSTOMERS_DATA)
    m_customers = load_customers(customers=c_data)

    return m_proxies, m_customers