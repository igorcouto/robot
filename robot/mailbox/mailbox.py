# Biult-in modules
import pytz
import socks
import email
from time import sleep
from datetime import datetime as dt, timedelta as td
from imaplib import IMAP4, IMAP4_SSL, IMAP4_SSL_PORT
# Third-party modules
import tzlocal
import lxml.html
# Project modules
from robot.mailbox.imap_ssl_by_proxy import IMAP_SSL_By_Proxy


def get_first_text_block(email_message):
    """Search on email message payload the first text type.

    Parameters
    ----------
    email_message : email.message.Message
        An instance of email.message.Message returned by a fetch.

    Return
    ------
    payload : str
        The text payload itself.
    """
    maintype = email_message.get_content_maintype()
    # If the first payload is multipart, go inside it
    if maintype == 'multipart':
        for part in email_message.get_payload():
            # Continues until find a text payload
            if part.get_content_maintype() == 'text':
                return part.get_payload()

    # If is a text payload, only returns
    elif maintype == 'text':
        return email_message.get_payload()

def convert_email_to_dict(email_message):
    """Converts an email message in a dictionary.

    Parameters
    ----------
    email_message : email.message.Message
        An instance of email.message.Message returned by a fetch.

    Return
    ------
    email_dict : dict
        The email in a dictionary format
    """
    email_dict = {}
    email_dict['from'] = email_message['From']
    email_dict['to'] = email_message['To']
    email_dict['subject'] = email_message['Subject']
    # Date
    email_dt = dt.strptime(email_message['Date'], '%a, %d %b %Y %H:%M:%S %z')
    email_dict['date'] = email_dt
    # Content
    email_content = get_first_text_block(email_message)
    email_dict['content'] = email_content

    return email_dict

class MailBox():
    """This class manager a mailbox using IMAP4 and SSL.
    """
    def __init__(self, email, password, imap_host, imap_port=IMAP4_SSL_PORT,
                 proxy_host=None, proxy_port=None, proxy_type='socks5'):
        """Initializes a MailBox object. To connect by proxy, you need provide
        proxy_host and proxy_port.
        """
        # Instance variables
        self.email = email
        self.password = password
        self.imap_host = imap_host
        self.imap_port = imap_port
        self.proxy_host = proxy_host
        self.proxy_port = proxy_port
        self.proxy_type = proxy_type

    def create_connection(self):
        """Creates a connection using IMAP4 protocol.

        Returns:
            connection -- an object to connect in a mailbox.
        """
        # If proxy host and port was providen, use proxy
        if self.proxy_host and self.proxy_port:
            conn = IMAP_SSL_By_Proxy(host=self.imap_host, port=self.imap_port,
                                     proxy_addr=self.proxy_host,
                                     proxy_port=self.proxy_port,
                                     proxy_type=self.proxy_type)
        else:
            conn = IMAP4_SSL(self.imap_host, self.imap_port)

        return conn

    def is_accessible(self):
        """Executes a log in and selects the Inbox folder to retrieve mailbox status
        and information.

        Returns
        -------
        result : dict
            A dictionary with status and mailbox_info.
        """
        status = 'success'
        info = None
        try:
            # Creates a connection
            conn = self.create_connection()

            # Login using instance conn object
            conn.login(user=self.email, password=self.password)

            # Selects Inbox folder and gets mailbox information
            conn.select('Inbox')
            info = conn.__dict__.copy()

            # Closes connection before leave
            conn.logout()
        except socks.ProxyConnectionError as e:
            status = e
        except IMAP4.error as e:
            status = e.args[0].decode()
        except socks.ProxyError as e:
            status = e
        except Exception as e:
            status = e

        return status, info

    def get_email_by_sender(self, sender, folder='Inbox'):
        """Gets all emails from sender.

        Arguments:
            sender {string} -- the email from sender

        Keyword Arguments:
            folder {str} -- [description] (default: {'Inbox'})
        """
        # Select the email folder
        conn = self.create_connection()
        conn.login(user=self.email, password=self.password)
        conn.select(folder)

        # Store emails in a list of dicts
        emails = []

        # Make the search based on subject
        search_status, search_data = conn.uid('search', None, '(FROM "%s")' % sender)
        if search_status == 'OK':

            # Fetch one by one
            for _id in search_data[0].split():
                fetch_status, fetch_data = conn.uid('fetch', _id, '(RFC822)')
                if fetch_status == 'OK':
                    email_message = email.message_from_bytes(fetch_data[0][1])
                    email_dict = convert_email_to_dict(email_message)
                    emails.append(email_dict)

        # Closes connection
        conn.logout()

        return emails

    def wait_for_code(self, attempts=10, time_bw_attempts=10):
        """Waits for the verification code arrives in customer mailbox.

        Parameters
        ----------
        attempts : int
            The number of times that the robot will attempt.
        time_bw_attempts : int
            The time between each attempt.

        Return
        ------
        code : str
            The verification code itself
        """
        # Tries n attemps times
        while (range(attempts)):
            # Current datetime
            now_dt = pytz.utc.localize(dt.utcnow())

            sleep(time_bw_attempts) # zZzZz...

            # Get emails from mailbox
            verification_emails = self.get_email_by_sender('account-update@amazon.com')
            verification_emails.reverse()

            for e in verification_emails:
                # Before compare datetime, convert to local timezone
                now_dt = now_dt.astimezone(tzlocal.get_localzone())
                email_dt = e['date'].astimezone(tzlocal.get_localzone())

                # Email datetime is less than 10 minutes
                if not now_dt - email_dt > td(minutes=5):
                    date_format = '%H:%M:%S on %Y/%m/%d'

                    # Convert content to HTML and parse using xpath to search the code.
                    content_in_html = lxml.html.fromstring(e['content'])
                    amz_code = content_in_html.xpath('//p[@class="otp"]/text()')[0]
                    return amz_code

        return None
