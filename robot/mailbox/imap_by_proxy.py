"""This code is an adaptation from https://gist.github.com/sstevan
Code: https://gist.github.com/sstevan/efccf3d5d3e73039c21aa848353ff52f
"""
# Built-in modules
import imaplib
# Third-party modules
import socks
from socks import create_connection


class IMAP_By_Proxy(imaplib.IMAP4):
    """
    IMAP service trough SOCKS proxy. PySocks module required.
    """

    PROXY_TYPES = {"socks4": socks.PROXY_TYPE_SOCKS4,
                   "socks5": socks.PROXY_TYPE_SOCKS5,
                   "http": socks.PROXY_TYPE_HTTP}

    def __init__(self, host, port=imaplib.IMAP4_PORT, proxy_addr=None, proxy_port=None,
                 rdns=True, username=None, password=None, proxy_type="socks5"):
        self.proxy_addr = proxy_addr
        self.proxy_port = proxy_port
        self.rdns = rdns
        self.username = username
        self.password = password
        self.proxy_type = IMAP_By_Proxy.PROXY_TYPES[proxy_type.lower()]

        imaplib.IMAP4.__init__(self, host, port)

    def _create_socket(self):
        return create_connection((self.host, self.port), proxy_type=self.proxy_type, proxy_addr=self.proxy_addr,
                                 proxy_port=self.proxy_port, proxy_rdns=self.rdns, proxy_username=self.username,
                                 proxy_password=self.password)