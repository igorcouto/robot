"""This code is an adaptation from https://gist.github.com/sstevan
Code: https://gist.github.com/sstevan/efccf3d5d3e73039c21aa848353ff52f
"""
# Built-in modules
import ssl
import imaplib
# Project modules
from robot.mailbox.imap_by_proxy import IMAP_By_Proxy


class IMAP_SSL_By_Proxy(IMAP_By_Proxy):
    """
    IMAP_SSL service trough SOCKS proxy
    """

    def __init__(self, host='', port=imaplib.IMAP4_SSL_PORT, keyfile=None, certfile=None,
                 ssl_context=None, proxy_addr=None, proxy_port=None, rdns=True,
                 username=None, password=None, proxy_type="socks5"):
        if ssl_context is not None and keyfile is not None:
            raise ValueError("ssl_context and keyfile arguments are mutually exclusive")
        if ssl_context is not None and certfile is not None:
            raise ValueError("ssl_context and certfile arguments are mutually exclusive")

        self.keyfile = keyfile
        self.certfile = certfile
        if ssl_context is None:
            ssl_context = ssl._create_stdlib_context(certfile=certfile,
                                                     keyfile=keyfile)
        self.ssl_context = ssl_context

        IMAP_By_Proxy.__init__(self, host, port, proxy_addr=proxy_addr, proxy_port=proxy_port,
                            rdns=rdns, username=username, password=password, proxy_type=proxy_type)

    def _create_socket(self):
        sock = IMAP_By_Proxy._create_socket(self)
        server_hostname = self.host if ssl.HAS_SNI else None
        return self.ssl_context.wrap_socket(sock, server_hostname=server_hostname)

    def open(self, host='', port=imaplib.IMAP4_PORT):
        IMAP_By_Proxy.open(self, host, port)
