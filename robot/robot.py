# Built-in libraries
import time
import logging
import multiprocessing
import logging.handlers
# project libraries
import settings
from robot.data import loader
from robot.logger import logger
from robot.utils import combine
from robot.userdata import compress
from robot.actions import visit_amazon
from robot.proxymonitor import microleaves


def configures_robot_logger():
    """Configures a logger system based in a multiprocessing.Queue. It is necessary
    because of multiprocesing and logging "incompatibility". Each module (process)
    need to have your own configure_logger method.

    Returns:
        Process, Queue -- Two objects. A process and a queue.
    """
    # Creates a process to control log messages.
    log_queue = multiprocessing.Queue(-1)
    log_process = multiprocessing.Process(target=logger.process, args=(log_queue,))

    # Handler to log in the prompt
    h = logging.handlers.QueueHandler(log_queue)
    root = logging.getLogger()
    root.addHandler(h)
    root.setLevel(logging.INFO)

    return log_process, log_queue

def execute_iteration(logger, pm_process, customers, log_queue):
    # Chooses proxy and customer based on region/state
    logger.info('Combining proxy and customer based on location.')
    proxy, customer = combine.choose_proxy_and_customer(proxymonitor=pm_process,
                                                        customers_manager=customers,
                                                        logger=logger)

    # Starts an action
    va_process = visit_amazon.VisitAmazonProcess(proxy=proxy,
                                                 customer=customer,
                                                 log_queue=log_queue)
    va_process.start()

    # MainProcess can terminate VisitAmazonProcess if proxy changes.
    while va_process.is_alive():
        # If IP changed, terminates visitamazon process
        if proxy.get('change_ip').is_set():
            logger.warn('Proxy %s changed. Terminanting VisitAmazonProcess.' % proxy.get('proxy'))
            logger.info('It\'s possible conclude another time.')
            va_process.terminate()
            break

    if va_process.finished_gracefully.is_set():
        # Change status to executed. Avoid re-use this customer.
        customer.update({'status': 'executed'})
    else:
        customer.update({'status': 'fail'})
        logger.warn('%s (%s) failed to access Amazon.' % (customer.get('name'), customer.get('email')))

    # Wait processes to finish
    va_process.join()

def run():
    """The main flow of the robot is here.
    """
    # Configures a log system to robot
    log_process, log_queue = configures_robot_logger()
    logger = logging.getLogger()
    log_process.start()

    logger.info('Started.')

    # Load proxies and customers
    PROXIES, CUSTOMERS = loader.load_CSV_to_managers()

    # Create and starts a ProxyMonitorProcess
    pm_process = microleaves.ProxyMonitorProcess(m_proxies=PROXIES, log_queue=log_queue)
    pm_process.start()

    # Starts a CompressUserDataDirs process
    cd = compress.CompressUserDataProcess(log_queue=log_queue)
    cd.start()

    # Waiting for a proxy
    logger.info('Waiting for ProxyMonitor to notify that proxies are available.')
    pm_process.available_proxies.wait()
    time.sleep(1)

    # Executes while a there are not used customers
    i = 1
    while len(list(filter(lambda c: c.get('status')=='unknow', CUSTOMERS))) > 0:
        logger.info('Iteration %s' % i)
        print('\n\n============================ ITERATION %s ==============================\n' %i)
        execute_iteration(logger=logger, pm_process=pm_process, customers=CUSTOMERS, log_queue=log_queue)
        i+= 1

    pm_process.terminate()
    cd.terminate()
    log_process.terminate()