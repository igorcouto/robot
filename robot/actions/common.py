# Built-in modules
import time
import random
import logging
# Third-party modules
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException


def random_sleep(min=3, max=10):
    """Randomly waits a time in order to proceed. Use after robot
    performs an action.

    Parameters
    ----------
    min : int
        The minimum time that bot will sleep.
    max : int
        The maximum time that bot will sleep.
    """
    if max <= min:
        max = min + 3
    time.sleep(random.randint(min, max))

def go_to_initial_page(driver):
    """Goes to initial page of Amazon."""
    try:
        driver.get('http://amazon.com')
        random_sleep() # zZzZzZz...
    except TimeoutException:
        pass

def clicks_in_nav_logo(driver):
    """Clicks in navigation logo of Amazon."""
    try:
        driver.find_element_by_id('nav-logo').click()
        random_sleep() # zZzZzZz...
    except (TimeoutException, NoSuchElementException):
        pass

def type_on_search_bar(driver, keyword):
    """Types a word on search bar."""
    logger = logging.getLogger('type_on_search_bar')
    logger.info('Keyword: %s' % keyword)
    try:
        search_textbox = driver.find_element_by_id('twotabsearchtextbox')
        search_textbox.clear()
        search_textbox.send_keys(keyword)
        random_sleep() # zZzZzZz...
        search_button = driver.find_element_by_xpath('//input[@value="Go"]')
        search_button.click()
        random_sleep() # zZzZzZz...
    except (TimeoutException, NoSuchElementException):
        pass

def empty_cart(driver):
    """Emptys cart"""
    wait = WebDriverWait(driver, 2)
    try:
        cart_count = wait.until(lambda d: d.find_element_by_id('nav-cart-count')).text
        if cart_count != '0':
            wait.until(lambda d: d.find_element_by_id('nav-cart')).click()
            wait.until(lambda d: d.find_element_by_id('sc-active-cart'))

            while cart_count != '0':
                products = wait.until(lambda d: d.find_elements_by_xpath('//*[@data-asin and not(@data-removed)]'))
                product = products.pop()
                asin = product.get_attribute('data-asin')
                product.find_element_by_xpath('.//input[@value="Delete"]').click()
                WebDriverWait(driver, 5).until(
                    EC.presence_of_element_located((By.XPATH,
                                                    '//*[@data-asin="%s" and @data-removed]' % asin))
                )
                cart_count = wait.until(lambda d: d.find_element_by_id('nav-cart-count')).text

        else:
            pass # No item found on cart
    except TimeoutException:
        pass

def click_on_next(driver):
    """Clicks on next button"""
    wait = WebDriverWait(driver, 10)
    try:
        wait.until(lambda d: d.find_element_by_xpath('//li[@class="a-last"]/a')).click()
        random_sleep() # zZzZz...
        return True
    except TimeoutException:
        return False

def get_status(elmt):
    """Search for "Sponsored" text inside an element.

    Parameters
    ----------
    elmt : str
        The status of element (sponsored or not).
    """
    status = 'non_sponsored'
    try:
        elmt.find_element_by_xpath('.//div[@data-component-type="sp-sponsored-result"]')
        status = 'sponsored'
    except NoSuchElementException:
        pass

    return status

def search_asins(driver, asins, page_limit=21):
    """Searches asins on listing. If a asin is found, clicks on product image."""
    wait = WebDriverWait(driver, 10)
    logger = logging.getLogger('search_asins')
    logger.info('ASIN: %s' % asins)
    pos_on_listing = 1
    for npage in range(1, page_limit + 1):
        # Filtering elements with products links
        listing = wait.until(lambda d: d.find_element_by_xpath('.//*[@data-component-type="s-search-results"]'))
        products = listing.find_elements_by_xpath('.//*[@data-asin]')
        for pos, elmt in enumerate(products, 1):
            # Gather product information
            elmt_asin = elmt.get_attribute('data-asin')
            elmt_status = get_status(elmt)

            if elmt_asin in asins:
                logger.info('%s found on page %s in position %s.' % (elmt_asin, npage, pos_on_listing))
                # Clicks on product image
                logger.info('Open product page.')
                elmt.find_element_by_xpath('.//*[@data-component-type="s-product-image"]/a').click()
                random_sleep() # zZzZz...
                return True

            # Controls the position of the product on listing
            pos_on_listing+=1

        # The page limit is reached
        if page_limit == npage:
            logger.info('Page limit (%s) reached.' % page_limit)
            break

        # Go to next page.
        if not click_on_next(driver):
            logger.info('Last page of the listing (%s) reached. Please choose another keyword.' % npage)
            break

    return False