# Built-in modules
import time
import random
import logging
# Third-party modules
import inflect
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains


def random_sleep(min=3, max=10):
    """Randomly waits a time in order to proceed. Use after robot
    performs an action.

    Parameters
    ----------
    min : int
        The minimum time that bot will sleep.
    max : int
        The maximum time that bot will sleep.
    """
    if max <= min:
        max = min + 3
    time.sleep(random.randint(min, max))

def see_images(driver):
    """Clicks on every thumbnail image of the product. Browser instance must in
    a product page to works."""
    wait = WebDriverWait(driver, 10)
    # Creates a logger
    logger = logging.getLogger('see_images')
    try:
        tumbimages = wait.until(
            lambda d: d.find_element_by_id('altImages')
        ).find_elements_by_xpath('//li[contains(@class, "imageThumbnail")]')
        # Log how many tumbnail images product has
        logger.info('Analyzing %s product images' % len(tumbimages))
        # Up to down or reversed
        if random.choice((True, False)):
            tumbimages.reverse()
        # Browser for all of them
        for img in tumbimages:
            random_sleep(min=1, max=2) # zZzZzZz...
            img.find_element_by_tag_name('input').click()
    except Exception:
        logger.info('No thumbnail images was found!')

def see_and_choose_a_color(driver):
    """Clicks on every color variation of the product. Browser instance must in a
    product page to works."""
    wait = WebDriverWait(driver, 10)
    # Creates a logger
    logger = logging.getLogger('see_and_choose_a_color')
    try:
        # Count how many color variations product has
        color_variations = wait.until(
            lambda d: d.find_element_by_id('variation_color_name')
        ).find_elements_by_xpath('//li[starts-with(@id, "color_name")]')
        logger.info('Analyzing %s color variations' % len(color_variations))

        # Left to right or reversed
        if random.choice((True, False)):
            color_variations.reverse()
        # Browser for all of them
        for color in color_variations:
            random_sleep() # zZzZzZz...
            color.click()
        random_sleep() # zZzZzZz...
        # Choose a color
        chosen_color = random.choice(color_variations)
        chosen_asin = chosen_color.get_attribute('data-defaultasin')
        logger.info('Product ASIN chosen: %s' % chosen_asin)
        chosen_color.click()
        random_sleep() # zZzZzZz...
    except Exception:
        logger.warning('No color variations found!')

def see_popup_reviews(driver):
    """Passes the mouse over the reviews popup. Browser instance must in a
    product page to works.
    """
    wait = WebDriverWait(driver, 10)
    # Creates a logger
    logger = logging.getLogger('see_popup_reviews')

    action = ActionChains(driver)
    try:
        reviews_popup = wait.until(lambda d: d.find_element_by_id('acrPopover'))
        action.move_to_element(reviews_popup).perform()
        logger.info('Seeing reviews popup')
        random_sleep(min=6) # zZzZzZz...
    except Exception:
        logger.info('Product does not have reviews yet.')
        pass

def see_reviews(driver):
    """Go to reviews section. The currentHeight and finalHeight returns
    can be used to scroll down and scroll up."""
    wait = WebDriverWait(driver, 10)
    # Creates a logger
    logger = logging.getLogger('see_reviews')

    try:
        # Go to reviews
        wait.until(lambda d: d.find_element_by_id('acrCustomerReviewLink')).click()
        logger.info('See reviews section')
        # Get reviews and calculate the beginning and end of element
        reviews = wait.until(lambda d: d.find_element_by_id('reviewsMedley'))
        currentHeight = driver.execute_script("return window.pageYOffset")
        finalHeight = currentHeight + reviews.size['height']
        random_sleep() # zZzZzZz...
        return currentHeight, finalHeight

    except Exception:
        logger.info('Product does not have a review section yet.')
        return None, None

def scroll_down(driver, currentHeight = 0, finalHeight = 0):
    """Makes the browser scrolls down."""
    # Creates a logger
    logger = logging.getLogger('scroll_down')

    logger.info('Scrolling down')
    if not currentHeight:
        currentHeight = driver.execute_script("return window.pageYOffset")
    if not finalHeight:
        finalHeight = driver.execute_script("return document.body.scrollHeight")
    if currentHeight < finalHeight: # final is not bigger then current height
        step = int((finalHeight - currentHeight)/20)
        for height in range(currentHeight, finalHeight, step)[0:-1]:
            random_sleep(min=2, max=4) # zZzZzZz...
            for sub_height in range(height, height+step, 20):
                driver.execute_script('window.scrollTo(0, %s);' % sub_height)

def scroll_up(driver, currentHeight = 0, finalHeight = 0):
    """Makes the browser scrolls up."""
    # Creates a logger
    logger = logging.getLogger('scroll_up')

    logger.info('Scrolling up')
    if not currentHeight:
        currentHeight = driver.execute_script("return window.pageYOffset")
    if not finalHeight:
        finalHeight = driver.execute_script("return document.body.scrollTop")
    if currentHeight > finalHeight: # final is not smaller then current height
        step = int((currentHeight - finalHeight)/20)
        for height in reversed(range(finalHeight, currentHeight, step)[0:-1]):
            random_sleep(min=2, max=4) # zZzZzZz...
            for sub_height in reversed(range(height, height+step, 20)):
                driver.execute_script('window.scrollTo(0, %s);' % sub_height)

def choose_quantity(driver):
    """Choose a quantity of product before insert in the cart. Browser
    instance must in a product page to works."""
    wait = WebDriverWait(driver, 10)
    # Creates a logger
    logger = logging.getLogger('choose_quantity')

    quantity = str(random.randint(1, 5))
    infl = inflect.engine()

    try:
        quantity_elmt = wait.until(lambda d: d.find_element_by_id('quantity'))
        select_qty = Select(quantity_elmt)
        select_qty.select_by_value(quantity)
        logger.info('Choosing %s %s.' % (quantity, infl.plural('item', quantity)))
    except Exception:
        logger.warning('Is not possible select quantity.')

    random_sleep(min=6) # zZzZzZz...

def add_to_cart(driver):
    """Choose a quantity of product before insert in the cart. Browser
    instance must in a product page to works."""
    wait = WebDriverWait(driver, 10)
    # Creates a logger
    logger = logging.getLogger('add_to_cart')

    try:
        wait.until(lambda d: d.find_element_by_id('add-to-cart-button')).click()
        logger.info('Adding to cart')
        random_sleep(min=6) # zZzZzZz...

        # Move the mouse to search bar to ignore some modals.
        action = ActionChains(driver)
        elmt_searchbox = driver.find_element_by_id('twotabsearchtextbox')
        action.move_to_element(elmt_searchbox).perform()
        action.click(elmt_searchbox).perform()

        return True
    except Exception:
        logger.info('"Add to cart" button did not appear.')

    return False