# Project module
from robot.actions import common
from robot.actions import product_page


def run(driver, keyword, asins):
    """Runs add to cart actions
    """
    common.go_to_initial_page(driver=driver)
    common.type_on_search_bar(driver=driver, keyword=keyword)
    search_result = common.search_asins(driver=driver,asins=asins)
    if search_result:
        product_page.see_images(driver)
        product_page.see_and_choose_a_color(driver)
        product_page.see_popup_reviews(driver)
        currentHeight, finalHeight = product_page.see_reviews(driver)
        product_page.scroll_down(driver, currentHeight, finalHeight)
        product_page.scroll_up(driver)
        product_page.choose_quantity(driver)
        add_to_cart_result = product_page.add_to_cart(driver)
    else:
        pass
