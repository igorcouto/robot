# Built-in modules
import time
import random
# Third-party modules
from random_word import RandomWords
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC


def __random_sleep(min=1, max=10):
    """Randomly waits a time in order to proceed. Use after robot
    performs an action.

    Parameters
    ----------
    min : int
        The minimum time that bot will sleep.
    max : int
        The maximum time that bot will sleep.
    """
    if max <= min:
        max = max + min
    time.sleep(random.randint(min, max))

def get_all_links(driver):
    """Gets all clickable links inside an Amazon page.

    Arguments:
        driver {selenium.webriver.Chrome} -- The webdriver instance to search.

    Returns:
        list -- A list of dictionaries.
    """
    links = []
    for elmt in driver.find_elements_by_xpath('.//a'):
        # It's a visible elmt
        if elmt.text.strip() != '':
            href = elmt.get_attribute('href')
            # There is a link inside amazon domain
            if href and href.strip() != '' and 'amazon.com' in href:
                link = {'elmt': elmt,
                        'href': href,
                        'text': elmt.text.replace('\n', ' ')}
                links.append(link)

    return links

def click_in_some_link(driver):
    """Clicks in some link in the page.

    Arguments:
        driver {selenium.webriver.Chrome} -- The webdriver instance to search.

    Returns:
        dict -- A dictionary with link informations
    """
    links = get_all_links(driver)
    link = random.choice(links)
    if 'amazon.com' in link['href']:
        driver.get(link['href'])
    else:
        go_to_main_page()

    __random_sleep() # zZzZz...

    return link

def go_to_main_page(driver):
    driver.get('https://amazon.com')
    __random_sleep() # zZzZz...

def click_on_nav_logo(driver):
    """Clicks on the logo of Amazon.

    Arguments:
        driver {selenium.webriver.Chrome} -- The webdriver instance to run the action.
    """
    # This action runs only in the Amazon domain
    if not 'amazon.com' in driver.current_url:
        go_to_main_page(driver)
    try:
        driver.find_element_by_id('nav-log').click()
    except NoSuchElementException:
        go_to_main_page(driver)

    __random_sleep() # zZzZz...

def search_product(driver):
    """Searchs some word

    Arguments:
        driver {[type]} -- [description]
    """
    r = RandomWords()
    word = r.get_random_word(hasDictionaryDef=True)
    # Fills search input element
    search_elmt = driver.find_element_by_id('twotabsearchtextbox')
    search_elmt.send_keys(word )
    __random_sleep() # zZzZz...

    # Clicks in Go button
    go_elmt = driver.find_element_by_xpath('//input[@value="Go"]')
    go_elmt.click()