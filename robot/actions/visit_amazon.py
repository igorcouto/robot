# Built-in libraries
import psutil
import logging
from time import sleep
import logging.handlers
from random import choice
from multiprocessing import Process, Manager, current_process
# Third-party modules
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
# Project modules
import settings
from robot.actions import browser
from robot.mailbox import mailbox


def configure_logger(log_queue):
    """Configures a logger based in a multiprocessing.Queue. It is necessary
    because of multiprocesing and logging "incompatibility". Each module
    (process) need to have your own configure_logger method.

    Arguments:
        log_queue {multiprocessing.Queue} -- An object (FIFO) to store the log
    """
    # Handler to log in the prompt
    h = logging.handlers.QueueHandler(log_queue)
    h.setLevel(logging.INFO)

    root = logging.getLogger()
    root.addHandler(h)

    # Set level of logger
    root.setLevel(logging.INFO)

class VisitAmazonProcess(Process):
    """VisitAmazonProcess class provides a process that a customer visit the Amazon website and 
    executes some action in order to keep account unsuspicius.
    """
    def __init__(self, proxy, customer, log_queue, name='VisitAmazon'):
        """Initializes a VisitAmazonProcess object
        """
        # Instance variables
        m = Manager()
        self.chromedriver_process = m.dict()
        self.proxy = proxy
        self.customer = customer
        self.log_queue = log_queue
        self.name = name
        self.finished_gracefully = m.Event()

        # Calls Process init
        Process.__init__(self, name=name)

    def update_chromedriver_process(self, driver):
        """Updates chromedriver_process Manager.dict() based on current driver
        instance.

        Parameters
        ----------
        driver : webdriver.Chrome()
            The driver instance
        """
        # Gets PID from process and children
        pid = driver.service.process.pid

        # Updates Manager dict
        self.chromedriver_process.update({'pid': pid})

    def wait_for_code(self):
        """Waits for Amazon Auth code.
        """
        c = self.customer
        p = self.proxy.get('proxy')
        m = mailbox.MailBox(email=c.get('email'), password=c.get('mailbox_password'),
                            imap_host=c.get('imap_host'), proxy_host=p.split(':')[0],
                            proxy_port=int(p.split(':')[1]))
        return m.wait_for_code()

    def execute_actions(self, driver, logger):
        """Here we place actions to be made.
        """
        driver.get('https://www.amazon.com/')

        if not browser.is_logged_in(driver=driver):
            logger.info('Logging in.')
            browser.open_login_page(driver=driver)
            browser.fill_login_form(driver=driver, email=self.customer.get('email'),
                                    password=self.customer.get('amazon_password'))

            # Authentication error message
            if browser.authentication_error(driver=driver):
                logger.warn('Authentication error message received.')
                self.customer.update({'status': 'auth error message'})
                return
            # Authentication warning message
            if browser.authentication_warning(driver=driver):
                logger.warn('Authentication warning message received.')
                self.customer.update({'status': 'auth warning message'})
                return
            # Anti-Auto Challenge
            if browser.anti_auto_challenge(driver=driver):
                logger.warn('Anti-Auto Challenge page received.')
                self.customer.update({'status': 'anti-auto challenge'})
                return

            if browser.verification_needed(driver=driver):
                logger.info('Verification page received.')
                code = self.wait_for_code()
                logger.info('Verification code received:: %s' % code)
                browser.enter_the_code(driver=driver, code=code)
        else:
            logger.info('Customer already logged in.')

        logger.info('Finishing actions.')
        # Notifies that this action finished gracefully.
        self.finished_gracefully.set()

    def run(self):
        """Overrides Process run.
        """
        # Configures logging module and creates a logger
        configure_logger(log_queue=self.log_queue)
        logger = logging.getLogger('VisitAmazon')
        logger.info('Started.')

        # Creates a driver instance to control a Chrome browser
        driver = browser.create_webdriver_Chrome(proxy=self.proxy, customer=self.customer,
                                                 executable_path=settings.CHROMEDRIVER_EXE,
                                                 logger=logger)

        # Updates chromedriver dict based on driver instance
        self.update_chromedriver_process(driver)

        # Executes actions
        try:
            self.execute_actions(driver=driver, logger=logger)
        except TimeoutException as e:
            logger.error('TimeoutException:: %s' % e.msg)
            logger.error('Page:: %s' % driver.current_url)
            logger.info('Slow proxy?? %s - latency in %s.' %(self.proxy.get('proxy'), self.proxy.get('info').get('latency')))

        logger.info('Closing browser.')
        driver.close()

    def terminate(self):
        """Overrides Process terminate. Before terminate process, use chromedriver_process
        to kill chromedriver.exe and chrome.exe
        """
        # Gets the process based on PID
        pid = self.chromedriver_process.get('pid')
        process = psutil.Process(pid)
        children = process.children()

        # Kills the children of driver instance.
        [psutil.Process(p.pid).kill() for p in children]

        # Kills the chromedriver process itself
        psutil.Process(pid).kill()

        # Calls Process terminate
        Process.terminate(self)