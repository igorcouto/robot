# Built-in modules
import os
import time
import random
# Third-party modules
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, TimeoutException
# Project modules
import settings
from robot.userdata import ziptools


def random_sleep(min=3, max=7):
    """Randomly waits a time in order to proceed. Use after robot
    performs an action.

    Parameters
    ----------
    min : int
        The minimum time that bot will sleep.
    max : int
        The maximum time that bot will sleep.
    """
    if max <= min:
        max = min + 3
    time.sleep(random.randint(min, max))

def create_ChromeOptions(proxy, customer):
    """Creates a Chrome option object based in the proxy and customer
    information.

    Parameters
    ----------
    proxy : dict
        An object with proxy information
    customer : dict
        An object with customer information

    Return
    ------
    opt : webdriver.ChromeOptions()
    """
    # Load options
    opt = webdriver.ChromeOptions()
    opt.add_argument('user-agent=%s' % customer.get('user_agent'))
    opt.add_argument('window-size=%s' % customer.get('resolution'))
    opt.add_argument('--lang=%s' % customer.get('languages'))

    # User-data directory
    temp_folder = os.path.abspath(settings.USERDATA_TEMP)
    opt.add_argument('user-data-dir=%s/%s' % (temp_folder,
                                              customer.get('email')))

    # Removing "Chrome is being controlled..."
    opt.add_experimental_option("excludeSwitches", ['enable-automation'])
    opt.add_experimental_option('useAutomationExtension', False)

    # Avoid to chrome shows a save password pop-up
    opt.add_experimental_option('prefs', {
        'credentials_enable_service': False,
        'profile': {
            'password_manager_enabled': False
        }
    })

    # Searching for extensions on chromedriver folder and loading them
    crx_files = []
    for r, d, f in os.walk('chromedriver'):
        for file in f:
            if '.crx' in file: crx_files.append(os.path.join(r, file))
    # Loading
    for f in crx_files:
        opt.add_extension(f)

    # Adding on ChromeOption object
    opt.add_argument('--proxy-server=%s' % proxy.get('proxy'))

    # Silent mode
    opt.add_argument("--log-level=3")

    return opt

def create_webdriver_Chrome(proxy, customer, executable_path, logger):
    """Creates a webdriver.Chrome() based on proxy and customer
    information

    Parameters
    ----------
    proxy : dict
        An object with proxy information
    customer : dict
        An object with customer information
    executable_path : string
        The path of chromedriver executable

    Return
    ------
    driver : webdriver.Chrome()
        The object to control a Chrome browser
    """
    # Creates ChromeOptions based on proxy and customer
    logger.info('Creating driver options.')
    options = create_ChromeOptions(proxy=proxy, customer=customer)

    # Unzips user-data dir on temp folder if exists one
    try:
        ziptools.unzip_user_data_dir(customer=customer, logger=logger)
    except ziptools.ZippedUserDataNotFoundError:
        pass

    # Instanciates driver to control the browser
    driver = webdriver.Chrome(executable_path=executable_path,
                              options=options)

    # Sets page load timeout to 30
    driver.set_page_load_timeout(30)

    return driver

def is_logged_in(driver):
    """Checks if customer is logged in.

    Parameters
    ----------
    driver : webdriver.Chrome()
        The object to control a Chrome browser

    Return
    ------
    logged_in : bool
        If True, the customer is logged in.
    """
    try:
        nav_link_accountList_elmt = driver.find_element_by_id('nav-link-accountList')
        nav_link_accountList_elmt.find_element_by_xpath('.//span[text()="Hello, Sign in"]')
    except NoSuchElementException:
        return True

    return False

def open_login_page(driver):
    """Goes to the login page.

    Return
    ------
    success : bool
        If True, the driver open the login page.
    """
    # Try to click on "Signin" floating button
    try:
        nav_signin_tooltip_elmt = WebDriverWait(driver, 2).until(
            EC.visibility_of_element_located((By.ID, 'nav-signin-tooltip'))
        )
        nav_signin_tooltip_elmt.find_element_by_xpath('//a[@data-nav-role="signin"]').click()
        random_sleep() # zZzZz...
        return True
    except:
        pass
    # Try to click on nav-tools bar
    try:
        driver.find_element_by_id('nav-link-accountList').click()
        random_sleep() # zZzZz...
        return True
    except TimeoutException:
        pass

def fill_login_form(driver, email, password):
    """Fills the login form with customer information.
    """
    # Insert email
    driver.find_element_by_id('ap_email').send_keys(email)
    random_sleep(min=1, max=5) # zZzZz...
    driver.find_element_by_id('continue').click()
    # Insert password
    random_sleep(min=1, max=5) # zZzZz...
    driver.find_element_by_id('ap_password').send_keys(password)
    driver.find_element_by_xpath('//input[@name="rememberMe"]').click()
    # Clicks to continue
    random_sleep(min=1, max=5) # zZzZz...
    driver.find_element_by_id('signInSubmit').click()
    random_sleep(min=1, max=5) # zZzZz...

def anti_auto_challenge(driver):
    """Verifies if Amazon shows Anti-Automation Challenge.

    Return
    ------
    success : bool
        If True, Amazon took the robot to verification page. Otherwise,
        go shopping.
    """
    # Anti-Automation Challenge?
    try:
        elmt = WebDriverWait(driver, 2).until(lambda d: d.find_element_by_id('cvf-page-content'))
        return True
    except TimeoutException:
        return False

def authentication_warning(driver):
    """Verifies if Amazon shows an authentication warning message.

    Return
    ------
    success : bool
        If True, Amazon responded with an authentication warning message.
    """
    # Anti-Automation Challenge?
    try:
        elmt = WebDriverWait(driver, 2).until(lambda d: d.find_element_by_id('auth-warning-message-box'))
        msg = elmt.text.replace('\n', ' ')
        return True
    except TimeoutException:
        return False

def authentication_error(driver):
    """Verifies if Amazon shows an authentication warning message.

    Return
    ------
    success : bool
        If True, Amazon responded with an authentication warning message.
    """
    # Anti-Automation Challenge?
    try:
        elmt = WebDriverWait(driver, 2).until(lambda d: d.find_element_by_id('auth-error-message-box'))
        msg = elmt.text.replace('\n', ' ')
        return True
    except TimeoutException:
        return False

def verification_needed(driver):
    """Verifies if Amazon shows a verification page. And clicks on "Continue"
    or "Send Code" button.

    Return
    ------
    success : bool
        If True, Amazon took the robot to verification page. Otherwise,
        go shopping.
    """
    # Verification needed?
    try:
        verify_form_xpath = '//form[@name="claimspicker" and @action="verify"]'
        WebDriverWait(driver, 2).until(lambda d: d.find_element_by_xpath(verify_form_xpath))
        random_sleep(min=1, max=5) # zZzZz...
        WebDriverWait(driver, 2).until(lambda d: d.find_element_by_id('continue')).click()
        return True
    except TimeoutException:
        return False

def enter_the_code(driver, code):
    """Enters the verification code.

    Parameters
    ----------
    code : str
        The code sent to customer email.
    """
    driver.find_element_by_xpath('//input[@name="code"]').send_keys(code)
    random_sleep(min=1, max=2) # zZzZz...
    driver.find_element_by_xpath('//input[@type="submit"]').click()