# Built-in modules
import time
import random
from multiprocessing import Manager
# Third-party modules
from selenium import webdriver
# Project modules
import settings


def create_ChromeOptions(**kwargs):
    """Creates an Options object to customize the browser. All settings are
    passed by kwargs (dictionary). Accepted arguments.
        + user-agent : str
        + window-size : str
        + --lang : str
        + user-data-dir : str
        + --proxy-server : str
        + --log-level : int
        + --headless : bool
        + disable-automation-extension : bool
        + password-manager : bool
        + extensions : list

    Returns:
        Options -- An object to customize a browser.
    """
    opt = webdriver.ChromeOptions()
    # Adding arguments
    if kwargs.get('user-agent'):
        opt.add_argument('user-agent=%s' % kwargs.get('user-agent'))
    if kwargs.get('window-size'):
        opt.add_argument('window-size=%s' % kwargs.get('window-size'))
    if kwargs.get('--lang'):
        opt.add_argument('--lang=%s' % kwargs.get('--lang'))
    if kwargs.get('user-data-dir'):
        opt.add_argument('user-data-dir=%s' % kwargs.get('user-data-dir'))
    if kwargs.get('--proxy-server'):
        opt.add_argument('--proxy-server=%s' % kwargs.get('--proxy-server'))
    if kwargs.get('--log-level'):
        opt.add_argument('--log-level=%s' % kwargs.get('--log-level'))
    if kwargs.get('headless'):
        opt.add_argument('--headless')
    # Adding experimental options
    if kwargs.get('disable-automation-extension'):
        opt.add_experimental_option("excludeSwitches", ['enable-automation'])
        opt.add_experimental_option('useAutomationExtension', False)
    if kwargs.get('password-manager'):
        opt.add_experimental_option('prefs', {'credentials_enable_service': False,
                                              'profile': {'password_manager_enabled': False}})
    # Adding extentions
    if kwargs.get('extensions'):
        extensions = kwargs.get('extensions')
        for ext in extensions:
            opt.add_extension(ext)

    return opt

class ChromeDriver():
    driver = driver_pid = None
    def __init__(self, executable_path=settings.CHROMEDRIVER_EXE, **kwargs):
        """ChromeDrives class provides a group of methods in order to control a driver instance.

        Keyword Arguments:
            executable_path {str} -- The path of the chrome driver executable (default: {settings.CHROMEDRIVER_EXE})
        """
        self.executable_path = executable_path
        M = Manager()
        self.is_available = M.Event()
        # Load options
        if kwargs.get('browser_settings'):
            self.opt = create_ChromeOptions(**kwargs.get('browser_settings'))
        else:
            self.opt = create_ChromeOptions()

    def initialize(self):
        """Initializes a webdriver.Chrome instance. Sets is_available event
        """
        self.driver = webdriver.Chrome(executable_path=self.executable_path, options=self.opt)
        self.driver.set_page_load_timeout(30)
        self.pid = self.driver.service.process.pid
        self.is_available.set()

    def execute(self, action):
        """Executes one or more actions (functions) over the driver instance.

        Arguments:
            action {dict} -- A dictionary or a list of dictionaries.
        """
        if self.is_available.is_set():
            if isinstance(action, list):
                for act in action:
                    func = act.get('func')
                    args = act.get('args')
                    if args:
                        func(self.driver, **args)
                    else:
                        func(self.driver)
                    time.sleep(1) # zZzZz
            else:
                func = action.get('func')
                args = action.get('args')
                if args:
                    func(self.driver, **args)
                else:
                    func(self.driver)

    def get(self, url):
        """Overrides get method of driver.

        Arguments:
            url {str} -- An URL to access.
        """
        if self.is_available.is_set():
            try:
                self.driver.get(url)
            except Exception:
                pass

    def quit(self):
        """Overrides quit method of driver.
        """
        try:
            self.driver.quit()
        except AttributeError:
            pass
        finally:
            self.pid = None
            self.is_available.clear()
