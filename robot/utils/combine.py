# Built-in modules
import time
import random
import logging
import logging.handlers
# Third-party modules
import inflect
# Project modules
from robot.mailbox import mailbox


def configure_logger(log_queue):
    """Configures a logger based in a multiprocessing.Queue. It is necessary
    because of multiprocesing and logging "incompatibility". Each module
    (process) need to have your own configure_logger method.

    Arguments:
        log_queue {multiprocessing.Queue} -- An object (FIFO) to store the log
    """
    # Handler to log in the prompt
    h = logging.handlers.QueueHandler(log_queue)
    formatter = logging.Formatter(
        '%(asctime)s | %(module)s | %(levelname)s | %(message)s',
        datefmt='%m/%d/%y %H:%M:%S'
    )
    h.setFormatter(formatter)
    h.setLevel(logging.INFO)

    root = logging.getLogger()
    root.addHandler(h)

    # Set level of logger
    root.setLevel(logging.INFO)

def check_mailbox(proxy, customer):
    """Checks if a mailbox is accessible by IMAP/proxy

    Parameters
    ----------
    proxy : dict
        The proxy data
    customer : dict
        The customer data

    Returns
    -------
    accessible : bool
        If True, is accessible
    """
    # Select proxy host and port
    proxy_host = proxy.get('proxy').split(':')[0]
    proxy_port = int(proxy.get('proxy').split(':')[1])
    # Creates a MailBox object to check
    m = mailbox.MailBox(email=customer.get('email'),
                        password=customer.get('mailbox_password'),
                        imap_host=customer.get('imap_host'),
                        proxy_host=proxy_host,
                        proxy_port=proxy_port)

    return m.is_accessible()

def combine_proxies_and_customers(proxies, customers, logger):
    """Combines a list of proxies against customers based by region/state.

    Arguments:
        proxies {list} -- The list of proxies.
        customers {list} -- The list of customers.
        logger {logging.getlogger} -- A logger to send log messages.

    Returns
    -------
    proxy, customer : tuple
        A tuple of proxy and customer
    """
    # Tries to combine all informed proxies
    for p in proxies:
        info = p.get('info')
        logger.info('Searching customers for %s - %s (%s/%s).' % (p.get('proxy'),
                                                                  info.get('query'),
                                                                  info.get('city'),
                                                                  info.get('regionName')))
        proxy = p.get('info')
        # Gets proxy region code
        proxy_region = proxy.get('region')
        # Filters customers by address_state and status
        candidates = [c for c in customers if c.get('address_state') == proxy_region and c.get('status') == 'unknow']
        # If there are candidates, returns only one
        if candidates:
            pl = inflect.engine()
            num = len(candidates)
            logger.info('%s %s found in %s.' % (num,
                                                pl.plural('customer', num),
                                                info.get('regionName')))
            return p, random.choice(candidates)
        # No candidates, try next proxy or return nothing
        else:
            logger.warning('No selected customers in %s.' % info.get('regionName'))

    return None, None

def choose_proxy_and_customer(proxymonitor, customers_manager, logger):
    """Chooses a proxy and a customer combining region/state of each other.
    After, the customer's mailbox is checked to avoid to use not useful
    customers. If fail, a new proxy/customer combination is made.

    Arguments:
        proxymonitor {ProxyMonitorProcess} -- An instance of ProxyMonitorProcess that
            monitor available proxies
        customers_manager {list} -- The list of customers
        logger {logging.getlogger} -- A logger to send log messages.

    Returns:
        tuple -- A tuple of proxy and customer
    """
    pl = inflect.engine()
    n_customers = len(list(filter(lambda c: c.get('status')=='unknow', customers_manager)))
    logger.info('%s %s to act - unknow status.' % (n_customers,
                                                   pl.plural('customer', n_customers)))

    # Executes until a good combination be made
    while True:
        # Gets available proxies
        available_proxies = proxymonitor.get_available_proxies()
        n_proxies = len(available_proxies)
        # Combines a proxy and a customer based on region/state
        proxy, customer = combine_proxies_and_customers(proxies=available_proxies,
                                                        customers=customers_manager,
                                                        logger=logger)

        # With proxy and customer choice, let's check if mailbox is accessible
        if customer and check_mailbox(proxy=proxy, customer=customer):
            info = proxy.get('info')
            logger.info('Proxy:: %s - %s (%s/%s/%s).' % (proxy.get('proxy'),
                                                         info.get('query'),
                                                         info.get('city'),
                                                         info.get('regionName'),
                                                         info.get('region')))
            logger.info('Customer:: %s - %s/%s (%s/%s).' % (customer.get('name'),
                                                            customer.get('email'),
                                                            customer.get('amazon_password'),
                                                            customer.get('address_city'),
                                                            customer.get('address_state')))

            # Nice! Break the loop
            break
        else:
            # Let's try again
            pass

        # Avoids a fast loop of messages when just a proxy is available and no customer/proxy
        # combination was founded.
        if not customer or not proxy:
            first_time = True
            while n_proxies >= len(proxymonitor.get_available_proxies()):
                if first_time:
                    pl = inflect.engine()
                    logger.info(
                        'There aren\'t customers for the %s available %s.' % (n_proxies,
                                                                              pl.plural('proxy', n_proxies))
                    )
                    first_time = False
                time.sleep(15)

    return proxy, customer
