# Built-in modules
import logging
import argparse
import multiprocessing
import logging.handlers
# Project modules
from robot.logger import logger
from robot import robot, all_actions, atc


def configures_robot_logger():
    """Configures a logger system based in a multiprocessing.Queue. It is necessary
    because of multiprocesing and logging "incompatibility". Each module (process)
    need to have your own configure_logger method.

    Returns:
        Process, Queue -- Two objects. A process and a queue.
    """
    # Creates a process to control log messages.
    log_queue = multiprocessing.Queue(-1)
    log_process = multiprocessing.Process(target=logger.process, args=(log_queue,))

    # Handler to log in the prompt
    h = logging.handlers.QueueHandler(log_queue)
    root = logging.getLogger()
    root.addHandler(h)
    root.setLevel(logging.INFO)

    return log_process, log_queue

def parse_cmd_arguments():
    """Parses the arguments passed by command line.

    Returns:
        [Namespace] -- An object take the options of the robot.
    """
    # Main parser
    parser = argparse.ArgumentParser(description='Script to run robot.',
                                     prog='python -m robot',
                                     usage='%(prog)s <prodecure> [options]')

    # Subparse to support several types of prodecures
    sbparser = parser.add_subparsers(title='prodecures',
                                     help='Select a prodecure to the robot execute.')
    sbparser.required = True
    sbparser.dest = 'prodecure'

    # Just visit procedure
    act_name = 'visit'
    act_msg = 'Procedure to visit Amazon combining customers and proxies - login and proxy monitoring.'
    visit_parser = sbparser.add_parser(act_name,
                                       description=act_msg,
                                       prog='python -m robot %s' % act_name,
                                       usage='%(prog)s [options]',
                                       help=act_msg)
    visit_parser.set_defaults(func=robot.run)

    # All prodecures procedure
    all_name = 'all-actions'
    all_msg = 'Procedure to run several actions in the Amazon - no logging or proxy monitoring.'
    all_parser = sbparser.add_parser(all_name,
                                     description=all_msg,
                                     prog='python -m robot %s' % all_name,
                                     usage='%(prog)s [options]',
                                     help=all_msg)
    all_parser.set_defaults(func=all_actions.run)

    # ATC procedure
    atc_name = 'atc'
    atc_msg = 'Procedure to simulate a "Add to cart" action in product page in Amazon.'
    atc_parser = sbparser.add_parser(atc_name,
                                     description=atc_msg,
                                     prog='python -m robot %s' % atc_name,
                                     usage='%(prog)s [options]',
                                     help=atc_msg)
    atc_parser.set_defaults(func=atc.run)

    # Populates namespace
    args = parser.parse_args()

    return args

if __name__ == '__main__':

    # Takes options received from command line.
    options = parse_cmd_arguments()

    # Configures a log process to handler log messages
    log_process, log_queue = configures_robot_logger()
    log_process.start()
    logger = logging.getLogger()

    logger.info('Started.')
    # Starts the robot
    options.func(log_queue=log_queue)

    # Terminates log process
    log_process.terminate()