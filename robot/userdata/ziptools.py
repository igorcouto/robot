# Built-in modules
import os
import re
import zipfile
# Third-party modules
import checksumdir
from hurry.filesize import size
#  Project modules
import settings


class UserDataDirLockedError(Exception):
    """Raised when it is not possible open user data dir.
    """
    def __init__(self, dir, logger):
        logger.error('It\'s not possible open userdata directory: %s.' % dir)

class ZippedUserDataNotFoundError(Exception):
    """Raised when there isn't a zipped user-data.
    """
    def __init__(self, email, logger):
        logger.error('Zipped user-data (%s) dir not found.' % email)

def unzip(file, dest_folder, logger):
    """Unzips a .zip file in a destination folder.

    Parameters
    ----------
    file {string} -- The .zip file to unzip
    dest_folder {string} -- The folder to store the unzipped filed.
    logger {logging} -- A logger to send log messages.
    """
    try:
        filepath = os.path.abspath(file)
        _zip = zipfile.ZipFile(filepath)
        _zip.extractall(os.path.join(dest_folder))
        _zip.close()
    except OSError as e:
        raise UserDataDirLockedError(os.path.basename(file), logger)

def unzip_user_data_dir(customer, logger):
    """Unzips the user-data directory in a temporary folder.

    Parameters
    ----------
    customer : dict
        A dictionary with all settings to unzips the user-data directory.
        logger {logging} - A logger to send log messages.
    """
    # Loads the folder to stored temporary files
    temp_folder = os.path.abspath(settings.USERDATA_TEMP)
    if not os.path.exists(temp_folder):
        logger.info('A temporary folder will be created in %s.' % temp_folder)
        os.mkdir(temp_folder)

    # Where user-data directory will be stored.
    temp_userdata = os.path.abspath(os.path.join(temp_folder, customer.get('email')))
    logger.info('Temp user-data will be stored in %s' % temp_userdata)

    # Loads where the profiles are stored in
    zipped_userdata_folder = os.path.abspath(r'D:\zipped')
    zipped_userdata = os.path.join(zipped_userdata_folder, customer.get('email')) + '.zip'
    if not os.path.exists(zipped_userdata):
        logger.error('%s not found in %s.' % (os.path.basename(zipped_userdata),
                                              zipped_userdata_folder))
        raise ZippedUserDataNotFoundError(customer.get('email'), logger)
    else:
        logger.info('%s found. Unziping...' % os.path.basename(zipped_userdata))
        unzip(zipped_userdata, temp_folder, logger)

def get_directories(root_dir):
    """Gets all sub-directories in a directory.

    Arguments:
        root_dir {string} -- The root directory.

    Returns:
        list -- A list that contains all directories absolute paths in root.
    """
    folders = []
    for subfolder in os.listdir(root_dir):
        subfolder_path = os.path.join(root_dir, subfolder)
        if os.path.isdir(subfolder_path):
            folders.append(subfolder_path)

    return folders

def get_hash(dir):
    """Gets the directory hash value.

    Arguments:
        dir {string} -- The directory to get hash.

    Returns:
        hash -- The hash value
    """
    dir_path = os.path.abspath(dir)
    try:
        h = checksumdir.dirhash(dir_path)
        return h
    except IOError:
        pass

def zip_folder(folder_path, output_path, logger):
    """Zips the contents of an entire folder (with that folder included
    in the archive). Empty subfolders will be included in the archive
    as well.

    Arguments:
        folder_path {string} -- The directory path
        output_path {string} -- The output directory path
        logger {logging} - A logger to send log messages.

    Returns:
        bool -- If true, the compress procedure was successful
    """
    parent_folder = os.path.dirname(folder_path)
    # Retrieve the paths of the folder contents.
    contents = os.walk(folder_path)
    try:
        zip_file = zipfile.ZipFile(output_path, 'w', zipfile.ZIP_DEFLATED)
        for root, folders, files in contents:
            if 'PepperFlash' in folders:
                folders.remove('PepperFlash')
            if 'Cache' in folders:
                folders.remove('Cache')
            if 'Code Cache' in folders:
                folders.remove('Code Cache')
            if 'ShaderCache' in folders:
                folders.remove('ShaderCache')

            # Include all subfolders, including empty ones.
            for folder_name in folders:
                absolute_path = os.path.join(root, folder_name)
                relative_path = absolute_path.replace(parent_folder + '\\', '')
                zip_file.write(absolute_path, relative_path)
            for file_name in files:
                if re.search('exe$', file_name):
                    continue
                absolute_path = os.path.join(root, file_name)
                relative_path = absolute_path.replace(parent_folder + '\\', '')
                try:
                    zip_file.write(absolute_path, relative_path)
                except ValueError as message:
                    logger.warning('Could not be zipped: %s.' % relative_path)

        filesize = size(os.path.getsize(output_path))
        logger.info("%s (%s) created successfully." % (os.path.basename(output_path),
                                                       filesize))
        zip_file.close()

        return True
    except IOError as message:
        logger.warning('IOError: %s to zip %s. Maybe in use.' % (message.strerror,
                                                                 os.path.basename(folder_path)))
    except OSError as message:
        logger.warning('OSError: %s to zip %s. Maybe is in use.' % (message.strerror,
                                                                    os.path.basename(folder_path)))
    except zipfile.BadZipfile as message:
        logger.warning('BadZipfile: Impossible to write in %s.' % os.path.basename(output_path) )

    return False