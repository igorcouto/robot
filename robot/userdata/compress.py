# Built-in modules
import os
import time
import shutil
import logging
import logging.handlers
from multiprocessing import Process
# Third-part modules
import inflect
# Project modules
import settings
from robot.userdata import ziptools


def configure_logger(log_queue, log_file):
    """Configures a logger based in a multiprocessing.Queue. It is necessary
    because of multiprocesing and logging "incompatibility". Each module
    (process) need to have your own configure_logger method.

    Arguments:
        log_queue {multiprocessing.Queue} -- An object (FIFO) to store the log
        log_file {string} -- The path to store additional log messages.
            messages.
    """
    # Handler to log in the prompt
    h = logging.handlers.QueueHandler(log_queue)
    h.setLevel(logging.INFO)

    # Handler to log in a file
    f = logging.FileHandler(log_file,  mode='a')
    formatter = logging.Formatter(
        '%(asctime)s | %(processName)s | %(levelname)s | %(message)s',
        datefmt='%m/%d/%y %H:%M:%S'
    )
    f.setFormatter(formatter)
    f.setLevel(logging.DEBUG)

    root = logging.getLogger()
    root.addHandler(h)
    root.addHandler(f)

    # Set level of logger
    root.setLevel(logging.INFO)

class CompressUserDataProcess(Process):
    """CompressUserDataProcess provides a thread (process) to compress Chrome user-data
    directories.
    """
    def __init__(self, log_queue):
        """Initializes CompressDirsProcess object.

        Arguments:
            log_queue {Queue} -- A multiprocessing.Queue object to send log messages
        """
        # Calls Process init
        Process.__init__(self, name='CompressUserData')

        # Instance variables
        self.source_dir = settings.USERDATA_TEMP
        self.destin_dir = settings.USERDATA_ZIPPED
        self.log_queue = log_queue
        self.log_file = settings.COMPRESS_USERDATA_LOG

    def run(self):
        """Overrides Process run.
        """
        # Configures logging module and creates a logger
        configure_logger(log_queue=self.log_queue, log_file=self.log_file)
        logger = logging.getLogger()
        logger.info('Started.')

        # Checks if the user-data dir and the zipped dir exist
        if not os.path.exists(self.source_dir):
            os.mkdir(self.source_dir)
        if not os.path.exists(self.destin_dir):
            os.mkdir(self.destin_dir)

        # Logs
        logger.info('SRC:: %s | DEST:: %s' % (self.source_dir, self.destin_dir))

        # How many user-data dirs exist in sources
        userdata_dir = ziptools.get_directories(self.source_dir)
        if userdata_dir:
            pl = inflect.engine()
            num = len(userdata_dir)
            logger.info('%s %s found in source.' % (num,
                                                    pl.plural('directory', num)))

        # To decide compress or not, the code first checks the hash code of a directory
        hashes = {}
        while True:
            # Compresses userdata folders
            for folder in ziptools.get_directories(self.source_dir):
                # If the returns None, skip to next folder
                current_hash = ziptools.get_hash(folder)
                if not current_hash:
                    continue
                # Checks if the folder is new
                if folder in hashes.keys():
                    # If didnt change, continue
                    if current_hash == hashes.get(folder):
                        continue
                    else:
                        # Directory has changed from the last check.
                        hashes[folder] = current_hash
                else:
                    # Directory not in hash keys.
                    hashes[folder] = current_hash

                zip_filename = os.path.basename(folder) + '.zip'
                zip_filepath = os.path.abspath(os.path.join(self.destin_dir,
                                                            zip_filename))
                # Compress the folder
                zipped = ziptools.zip_folder(folder, zip_filepath, logger)

                # If the zip file is created successfully, delete the temp folder
                try:
                    if zipped:
                        shutil.rmtree(folder)
                except PermissionError:
                    logger.error('Impossible to delete %s' % folder)

            # Waits a little to repeat.
            time.sleep(20)