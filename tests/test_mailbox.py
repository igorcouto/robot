# Project settings
from robot.mailbox import mailbox


def test_mailbox():
    """Tests if Mailbox class works properly with no proxy."""
    mb = mailbox.MailBox(email='campbellkylie87@yahoo.com',
                         password='QeYrD8n7f',
                         imap_host='imap.mail.yahoo.com')
    assert mb.is_accessible(), 'mailbox is not accessible.'

def test_mailbox_over_proxy():
    """Tests if Mailbox class works properly over a proxy."""
    mb = mailbox.MailBox(email='campbellkylie87@yahoo.com',
                         password='QeYrD8n7f',
                         imap_host='imap.mail.yahoo.com',
                         proxy_host='163.172.111.11',
                         proxy_port='1231')
    assert mb.is_accessible(), 'mailbox is not accessible over a proxy.'
