# Built-in libraries
from threading import Thread
from datetime import datetime
from json import JSONDecodeError
from multiprocessing import Manager
from multiprocessing.managers import EventProxy
# Third-party libraries
import mock
from requests.exceptions import ProxyError
# Project libraries
import settings
from robot.proxymonitor import microleaves
from tests import mocks_proxymonitor as mock_pm


def create_proxy_manager(proxy_host):
    """Creates a multiprocessing.Manager().dict()
    """
    m = Manager()
    m_proxy = m.dict({'proxy': proxy_host,
                      'available': m.Event(),
                      'change_ip': m.Event(),
                      'counter': 0,
                      'info': {},
                      'refresh': settings.PROXY_REFRESH_TIME})
    return m_proxy

@mock.patch('robot.proxymonitor.microleaves.requests.get', side_effect=mock_pm.mocked_requests_get)
def test_request_to_ip_api(mock_requests_get):
    """Tests if request_to_ip_api works properly in a direct connection - no proxy.
    """
    proxy_info = microleaves.request_to_ip_api(proxy_host=None)
    assert bool(proxy_info), 'request_to_ip_api returns nothing.'
    assert isinstance(proxy_info, dict), 'response isn\'t a dictionary.'
    keys = ['query', 'status', 'country', 'countryCode', 'region', 'regionName', 'city', 'latency']
    for k in keys:
        assert k in proxy_info, '%s not in response'

@mock.patch('robot.proxymonitor.microleaves.requests.get', side_effect=mock_pm.mocked_requests_get)
def test_request_to_ip_api_over_proxy(mock_requests_get):
    """Tests if request_to_ip_api works properly over a proxy
    """
    proxy_info = microleaves.request_to_ip_api(proxy_host='some PROXY HOST')
    assert bool(proxy_info), 'request_to_ip_api returns nothing.'
    assert isinstance(proxy_info, dict), 'response isn\'t a dictionary.'
    keys = ['query', 'status', 'country', 'countryCode', 'region', 'regionName', 'city', 'latency']
    for k in keys:
        assert k in proxy_info, '%s not in response' % k

@mock.patch('robot.proxymonitor.microleaves.request_to_ip_api', return_value=mock_pm.PROXY_INFO_01)
def test_get_proxy_info_response(mock_requests_to_ip_api):
    """Tests if get_proxy_info responses properly.
    """
    proxy_info = microleaves.get_proxy_info(proxy_host='some PROXY HOST')
    assert bool(proxy_info), 'request_to_ip_api returns nothing.'
    assert isinstance(proxy_info, dict), 'response isn\'t a dictionary.'

@mock.patch('robot.proxymonitor.microleaves.requests.get')
def test_get_proxy_info_handlers_exceptions(mock_requests_to_ip_api):
    """Tests if get_proxy_info handlers exceptions.
    """
    exception_msg = 'fail'
    mock_requests_to_ip_api.side_effect = Exception(exception_msg)
    fail_proxy_info = microleaves.get_proxy_info(proxy_host='some PROXY HOST')
    assert 'status' in fail_proxy_info, 'status not in response.'
    assert exception_msg == fail_proxy_info.get('status'), 'exception message not captured.'

@mock.patch('robot.proxymonitor.microleaves.requests.get')
def test_get_proxy_info_handlers_JSONDecodeError(mock_requests_to_ip_api):
    """Tests if get_proxy_info handlers JSONDecodeError.
    """
    exception_msg = 'JSONDecodeError'
    mock_requests_to_ip_api.side_effect = JSONDecodeError(msg=exception_msg,
                                                          doc='', pos=1)
    fail_proxy_info = microleaves.get_proxy_info(proxy_host='some PROXY HOST')
    assert 'status' in fail_proxy_info, 'status not in response.'
    assert exception_msg == fail_proxy_info.get('status'), 'exception message not captured.'

@mock.patch('robot.proxymonitor.microleaves.requests.get')
def test_get_proxy_info_handlers_ProxyError(mock_requests_to_ip_api):
    """Tests if get_proxy_info handlers ProxyError.
    """
    exception_msg = 'ProxyError'
    mock_requests_to_ip_api.side_effect = ProxyError(exception_msg)
    fail_proxy_info = microleaves.get_proxy_info(proxy_host='some PROXY HOST')
    assert 'status' in fail_proxy_info, 'status not in response.'
    assert exception_msg == fail_proxy_info.get('status'), 'exception message not captured.'

@mock.patch('robot.proxymonitor.microleaves.get_proxy_info', return_value=mock_pm.PROXY_INFO_01)
def test_update_proxy_response(mock_get_proxy_info):
    """Tests if update_proxy responses properly.
    """
    m_proxy = create_proxy_manager(proxy_host='some PROXY HOST')
    microleaves.update_proxy(m_proxy=m_proxy)
    keys = ['info', 'available', 'last_update', 'change_ip', 'counter']
    for k in keys:
        assert k in m_proxy, '%s not in response.' % k
        if k == 'available' or k == 'change_ip':
            assert isinstance(m_proxy.get(k), EventProxy), '%s isn\'t a Event type' % k
        if k == 'last_update':
            assert isinstance(m_proxy.get(k), datetime), '%s isn\'t a datetime type' % k

@mock.patch('robot.proxymonitor.microleaves.get_proxy_info')
def test_update_proxy_based_on_location(mock_get_proxy_info):
    """Tests if update_proxy sets available event properly based on location. Non-USA IP's couldn't put
    proxy in available state.
    """
    # Mocks three proxy changes
    mock_get_proxy_info.side_effect = [mock_pm.PROXY_INFO_01,
                                       mock_pm.PROXY_INFO_02]
    m_proxy = create_proxy_manager(proxy_host='some PROXY HOST')
    # Non-USA IP
    microleaves.update_proxy(m_proxy=m_proxy)
    assert not m_proxy.get('available').is_set(), 'available event was set with a non-USA IP.'
    # USA IP
    microleaves.update_proxy(m_proxy=m_proxy)
    assert m_proxy.get('available').is_set(), 'available event was not set with a USA IP.'

@mock.patch('robot.proxymonitor.microleaves.get_proxy_info')
def test_update_proxy_available_based_on_status(mock_get_proxy_info):
    """Tests if update_proxy sets available event based in status properly.
    """
    m_proxy = create_proxy_manager(proxy_host='some PROXY HOST')
    # If status is success available need to be set
    mock_get_proxy_info.return_value = mock_pm.PROXY_INFO_02
    microleaves.update_proxy(m_proxy=m_proxy)
    assert m_proxy.get('available').is_set(), 'available is False even when status is success.'
    # If status is different of success, available event couldn't be set.
    mock_pm.PROXY_INFO_02.update({'status': 'ProxyError'})
    m_proxy = create_proxy_manager(proxy_host='some PROXY HOST')
    mock_get_proxy_info.return_value = mock_pm.PROXY_INFO_02
    microleaves.update_proxy(m_proxy=m_proxy)
    assert not m_proxy.get('available').is_set(), 'available is True even when status is bad.'

@mock.patch('robot.proxymonitor.microleaves.get_proxy_info')
def test_update_proxy_last_update(mock_get_proxy_info):
    """Tests if update_proxy sets last_update properly.
    """
    # Mocks three proxy changes
    mock_get_proxy_info.side_effect = [mock_pm.PROXY_INFO_01,
                                       mock_pm.PROXY_INFO_01,
                                       mock_pm.PROXY_INFO_02]
    m_proxy = create_proxy_manager(proxy_host='some PROXY HOST')
    # First update
    microleaves.update_proxy(m_proxy=m_proxy)
    # Second update - need to change last_update
    previous_last_update = m_proxy.get('last_update')
    microleaves.update_proxy(m_proxy=m_proxy)
    assert previous_last_update == m_proxy.get('last_update'), 'last_update changed even when IP didn\'t.'
    # Third update - need to change last_update
    previous_last_update = m_proxy.get('last_update')
    microleaves.update_proxy(m_proxy=m_proxy)
    assert previous_last_update != m_proxy.get('last_update'), 'last_update didn\'t change even when IP did.'

@mock.patch('robot.proxymonitor.microleaves.get_proxy_info')
def test_update_proxy_change_ip_event(mock_get_proxy_info):
    """Tests if update_proxy updates change_ip event properly.
    """
    # Mocks four proxy changes
    mock_get_proxy_info.side_effect = [mock_pm.PROXY_INFO_02,
                                       mock_pm.PROXY_INFO_02,
                                       mock_pm.PROXY_INFO_01,
                                       mock_pm.PROXY_INFO_01]
    m_proxy = create_proxy_manager(proxy_host='some PROXY HOST')
    microleaves.update_proxy(m_proxy=m_proxy)
    assert not m_proxy.get('change_ip').is_set(), 'change_ip event was set even when IP haven\'t change.'
    # Simulates a second update with no change in IP
    microleaves.update_proxy(m_proxy=m_proxy)
    assert not m_proxy.get('change_ip').is_set(), 'change_ip event was set even when IP haven\'t change.'
    # Simulates a change of IP
    microleaves.update_proxy(m_proxy=m_proxy)
    assert m_proxy.get('change_ip').is_set(), 'change_ip wasn\'t set even when IP have changed.'
    # Simulates a forth update with no change in IP
    microleaves.update_proxy(m_proxy=m_proxy)
    assert not m_proxy.get('change_ip').is_set(), 'change_ip event was set even when IP haven\'t change.'

@mock.patch('robot.proxymonitor.microleaves.get_proxy_info')
def test_update_proxy_counter_behavior(mock_get_proxy_info):
    """Tests if update_proxy increments counter properly.
    """
    # Mocks four proxy changes
    mock_get_proxy_info.side_effect = [mock_pm.PROXY_INFO_01,
                                       mock_pm.PROXY_INFO_02,
                                       mock_pm.PROXY_INFO_02,
                                       mock_pm.PROXY_INFO_01]
    m_proxy = create_proxy_manager(proxy_host='some PROXY HOST')
    microleaves.update_proxy(m_proxy=m_proxy)
    assert m_proxy.get('counter') == 1, 'counter isn\'t 1 on the first change.'
    microleaves.update_proxy(m_proxy=m_proxy)
    assert m_proxy.get('counter') == 2, 'counter isn\'t 2 on the second change.'
    microleaves.update_proxy(m_proxy=m_proxy)
    assert m_proxy.get('counter') == 2, 'counter should be 2 because IP didn\'t change.'
    microleaves.update_proxy(m_proxy=m_proxy)
    assert m_proxy.get('counter') == 3, 'counter isn\'t 3 on the third change.'

@mock.patch('robot.proxymonitor.microleaves.get_proxy_info')
def test_update_proxy_refresh_behavior(mock_get_proxy_info):
    """Tests if update_proxy increments/decreases the proxy refresh time properly.
    """
    # Mocks four proxy changes
    mock_get_proxy_info.side_effect = [mock_pm.PROXY_INFO_01, # Non USA IP
                                       mock_pm.PROXY_INFO_02] # USA IP
    m_proxy = create_proxy_manager(proxy_host='some PROXY HOST')
    mock_queue = mock.MagicMock()
    mock_queue.info.return_value = None
    # Increases refresh
    microleaves.update_proxy(m_proxy=m_proxy, logger=mock_queue)
    high_refresh = settings.HIGH_PROXY_REFRESH_TIME
    assert m_proxy.get('refresh') == high_refresh, 'refresh didn\'t increase with a non-USA IP.'
    # Decreases refresh
    microleaves.update_proxy(m_proxy=m_proxy, logger=mock_queue)
    refresh = settings.PROXY_REFRESH_TIME
    assert m_proxy.get('refresh') == refresh, 'refresh didn\'t decrease with a USA IP.'

@mock.patch('robot.proxymonitor.microleaves.get_proxy_info')
def test_update_proxy_in_parallel(mock_get_proxy_info):
    """Tests if update_proxy can be parallelized.
    """
    # Mocks two update_proxy responses
    mock_get_proxy_info.side_effect = [mock_pm.PROXY_INFO_01,
                                       mock_pm.PROXY_INFO_02]
    m_proxies = [create_proxy_manager(proxy_host='some PROXY HOST'),
                 create_proxy_manager(proxy_host='some PROXY HOST')]

    thread_pool = []
    for proxy in m_proxies:
        t = Thread(target=microleaves.update_proxy, args=(proxy,))
        thread_pool.append(t)

    # Starts threads
    [t.start() for t in thread_pool]

    # Finishes threads
    [t.join() for t in thread_pool]

    for proxy in m_proxies:
        assert proxy.get('info'), 'thread didn\'t update a proxy.'