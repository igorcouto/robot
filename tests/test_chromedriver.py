# Built-in modules
import os
import mock
# Third-party modules
import pytest
from selenium.webdriver.chrome.options import Options
# Project modules
import settings
from robot.actions import common
from robot.driver import chromedriver


BROWSER_SETTINGS = {'user-agent': 'SOME USER AGENT',
                    'window-size': 'SOME SIZE',
                    '--lang': 'SOME LANG',
                    'user-data-dir': 'SOME DIR',
                    '--proxy-server': 'SOME PROXY',
                    '--log-level': 3,
                    'disable-automation-extension': True,
                    'password-manager': True,
                    'extensions': []}

def test_create_ChromeOptions_returns_options():
    opt = chromedriver.create_ChromeOptions()
    assert opt, 'the function returns nothing.'
    assert isinstance(opt, Options), 'the functions didn\'t return an Option type.'

def test_create_ChromeOptions_set_user_agent():
    browser_settings = {'user-agent': 'SOME BROWSER'}
    opt = chromedriver.create_ChromeOptions(**browser_settings)
    assert 'user-agent=SOME BROWSER' in opt.arguments

def test_create_ChromeOptions_set_window_size():
    browser_settings = {'window-size': 'SOME SIZE'}
    opt = chromedriver.create_ChromeOptions(**browser_settings)
    assert 'window-size=SOME SIZE' in opt.arguments

def test_create_ChromeOptions_set_language():
    browser_settings = {'--lang': 'SOME LANG'}
    opt = chromedriver.create_ChromeOptions(**browser_settings)
    assert '--lang=SOME LANG' in opt.arguments

def test_create_ChromeOptions_set_user_data_dir():
    browser_settings = {'user-data-dir': 'SOME DIR'}
    opt = chromedriver.create_ChromeOptions(**browser_settings)
    assert 'user-data-dir=SOME DIR' in opt.arguments

def test_create_ChromeOptions_set_proxy_server():
    browser_settings = {'--proxy-server': 'SOME PROXY SERVER'}
    opt = chromedriver.create_ChromeOptions(**browser_settings)
    assert '--proxy-server=SOME PROXY SERVER' in opt.arguments

def test_create_ChromeOptions_set_log_level():
    browser_settings = {'--log-level': 2}
    opt = chromedriver.create_ChromeOptions(**browser_settings)
    assert '--log-level=2' in opt.arguments

def test_create_ChromeOptions_set_headless():
    browser_settings = {'headless': True}
    opt = chromedriver.create_ChromeOptions(**browser_settings)
    assert '--headless' in opt.arguments

def test_create_ChromeOptions_set_automation_extension():
    automation_extension = {'excludeSwitches': ['enable-automation'],
                            'useAutomationExtension': False}
    # Enabling automation extension
    opt = chromedriver.create_ChromeOptions()
    assert not automation_extension == opt.experimental_options
    # Disabling automation extension
    browser_settings = {'disable-automation-extension': True}
    opt = chromedriver.create_ChromeOptions(**browser_settings)
    assert automation_extension == opt.experimental_options

def test_create_ChromeOptions_set_password_manager():
    password_manager = {'prefs': {'credentials_enable_service': False,
                                  'profile': {'password_manager_enabled': False}}}
    # Enabling password manager
    opt = chromedriver.create_ChromeOptions()
    assert not password_manager == opt.experimental_options
    # Disabling password manager
    browser_settings = {'password-manager': True}
    opt = chromedriver.create_ChromeOptions(**browser_settings)
    assert password_manager == opt.experimental_options

def test_create_ChromeOptions_add_extensions():
    # No extension added
    opt = chromedriver.create_ChromeOptions()
    assert len(opt.extensions) == 0
    # Adding a extension
    block_img = os.path.join(settings.CHROMEDRIVER_DIR, 'block_img.crx')
    browser_settings = {'extensions': [block_img]}
    opt = chromedriver.create_ChromeOptions(**browser_settings)
    assert len(opt.extensions) == 1
    # Adding 2 extensions
    webrtc_control = os.path.join(settings.CHROMEDRIVER_DIR, 'webrtc-control.crx')
    browser_settings = {'extensions': [block_img, webrtc_control]}
    opt = chromedriver.create_ChromeOptions(**browser_settings)
    assert len(opt.extensions) == 2

def test_ChromeDriver_driver_starts_unavailable():
    driver = chromedriver.ChromeDriver()
    assert not driver.is_available.is_set(), 'is_available is set True by default.'

@mock.patch('selenium.webdriver.Chrome')
def test_ChromeDriver_initialize_creates_driver(mock_webdriver_chrome):
    driver = chromedriver.ChromeDriver()
    driver.initialize()
    mock_webdriver_chrome.assert_called_once()

@mock.patch('selenium.webdriver.Chrome')
def test_ChromeDriver_initialize_sets_is_available(mock_webdriver_chrome):
    driver = chromedriver.ChromeDriver()
    assert hasattr(driver, 'is_available'), 'there isn\'t a is_available attr.'
    driver.initialize()
    assert driver.is_available.is_set(), 'initialize doesn\'t set is_available.'

@mock.patch('selenium.webdriver.Chrome')
@mock.patch.object(chromedriver.ChromeDriver, 'driver')
def test_ChromeDriver_quit_clears_is_available(mock_chromedriver, mock_webdriver_chrome):
    driver = chromedriver.ChromeDriver()
    driver.initialize()
    driver.quit()
    assert not driver.is_available.is_set(), 'quit doesn\'t clear is_available.'

@mock.patch('selenium.webdriver.Chrome')
def test_ChromeDriver_initialize_sets_driver_pid(mock_driver):
    driver = chromedriver.ChromeDriver()
    driver.initialize()
    assert driver.pid, 'initialize doesn\'t update pid'

@mock.patch('robot.driver.chromedriver.create_ChromeOptions')
@mock.patch('selenium.webdriver.Chrome')
def test_ChromeDriver_initialize_with_no_args(mock_driver, mock_create_chromeoptions):
    opt = chromedriver.create_ChromeOptions()
    mock_create_chromeoptions.return_value = opt
    driver = chromedriver.ChromeDriver()
    driver.initialize()
    mock_driver.assert_called_with(executable_path=settings.CHROMEDRIVER_EXE, options=opt)

@mock.patch('robot.driver.chromedriver.create_ChromeOptions')
@mock.patch('selenium.webdriver.Chrome')
def test_ChromeDriver_initialize_with_browser_settings(mock_driver, mock_create_chromeoptions):
    browser_settings = {'user-agent': 'SOME BROWSER'}
    opt = chromedriver.create_ChromeOptions(**browser_settings)
    mock_create_chromeoptions.return_value = opt
    driver = chromedriver.ChromeDriver(browser_settings=browser_settings)
    driver.initialize()
    mock_driver.assert_called_with(executable_path=settings.CHROMEDRIVER_EXE, options=opt)

@mock.patch('selenium.webdriver.Chrome')
def test_ChromeDriver_quit_sets_driver_pid_None(mock_driver):
    driver = chromedriver.ChromeDriver()
    driver.initialize()
    driver.quit()
    assert not driver.driver_pid, 'quit doesn\'t set driver_pid to None'

def test_ChromeDriver_quit_avoids_exceptions():
    driver = chromedriver.ChromeDriver()
    try:
        driver.quit()
    except AttributeError:
        pytest.fail('close an uninitialized driver launches AttributeError.')

def test_ChromeDriver_get_avoids_AttributeError():
    driver = chromedriver.ChromeDriver()
    try:
        driver.get('SOME URL')
    except AttributeError:
        pytest.fail('get an URL with an uninitialized driver launches AttributeError.')

@mock.patch('selenium.webdriver.Chrome.get', side_effect=ConnectionResetError)
def test_ChromeDriver_get_avoids_Exception(mock_get):
    driver = chromedriver.ChromeDriver()
    try:
        driver.get('SOME URL')
    except Exception:
        pytest.fail('get doesn\'t handle Exception.')

def test_ChromeDriver_execute_action_driver_available():
    driver = chromedriver.ChromeDriver()
    with mock.patch.object(chromedriver.ChromeDriver, 'initialize'):
        driver.initialize()
        driver.is_available.set()

    func = mock.Mock()
    action = {'func': func}
    driver.execute(action)
    func.assert_called_once()

def test_ChromeDriver_execute_action_driver_not_available():
    driver = chromedriver.ChromeDriver()
    with mock.patch.object(chromedriver.ChromeDriver, 'initialize'):
        driver.initialize()
        driver.is_available.clear()

    func = mock.Mock()
    action = {'func': func}
    driver.execute(action)
    func.assert_not_called()

def test_ChromeDriver_execute_action_list_as_arg():
    driver = chromedriver.ChromeDriver()
    with mock.patch.object(chromedriver.ChromeDriver, 'initialize'):
        driver.initialize()
        driver.is_available.set()

    func1 = mock.Mock()
    func2 = mock.Mock()
    func3 = mock.Mock()
    actions = [{'func': func1}, {'func': func2}, {'func': func3}]
    driver.execute(actions)
    for action in actions:
        action.get('func').assert_called_once()

def test_ChromeDriver_execute_action_function_with_args():
    driver = chromedriver.ChromeDriver()
    with mock.patch.object(chromedriver.ChromeDriver, 'initialize'):
        driver.initialize()
        driver.is_available.set()

    func = mock.Mock()
    action = {'func': func, 'args':{'word': 'home'}}
    driver.execute(action)
    func.assert_called_with(None, **action.get('args'))
