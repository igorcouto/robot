PROXY_HOST = '163.172.111.11:1231'
PROXY_INFO_01 = {'status': 'success',
                 'country': 'Brazil',
                 'countryCode': 'BR',
                 'region': 'PA',
                 'regionName': 'Para',
                 'city': 'Belém',
                 'zip': '66000',
                 'lat': -1.4558,
                 'lon': -48.5044,
                 'timezone': 'America/Belem',
                 'isp': 'TIM S/A',
                 'org': 'TIM CELULAR S.A',
                 'as': 'AS26615 TIM S/A',
                 'query': '189.40.105.181',
                 'latency': 1.3133659362792969}
PROXY_INFO_02 = {'status': 'success',
                'country': 'United States',
                'countryCode': 'US',
                'region': 'AR',
                'regionName': 'Arkansas',
                'city': 'Little Rock',
                'zip': '72205',
                'lat': 34.7456,
                'lon': -92.3419,
                'timezone': 'America/Chicago',
                'isp': 'AT\u0026T Services, Inc.',
                'org': 'AT\u0026T Corp',
                'as': 'AS7018 AT\u0026T Services, Inc.',
                'query': '172.125.21.255',
                'latency': 1.3133659362792969}

class MockIPAPIResponse:
    def __init__(self, json_data, status_code):
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        return self.json_data

def mocked_requests_get(*args, **kwargs):
    """This method will be used by the mock to replace requests.get()

    Returns:
        [MockIPAPIResponse] -- A mocked response
    """
    dict_r = {'status': 'success', 'country': 'Brazil', 'countryCode': 'BR',
              'region': 'PA', 'regionName': 'Para', 'city': 'Belém',
              'zip': '66000', 'lat': -1.4558, 'lon': -48.5044,
              'timezone': 'America/Belem', 'isp': 'TIM S/A',
              'org': 'TIM CELULAR S.A', 'as': 'AS26615 TIM S/A',
              'query': '189.40.105.181'}

    if args[0] == 'http://ip-api.com/json':
        return MockIPAPIResponse(dict_r, 200)

    return MockIPAPIResponse(None, 404)
