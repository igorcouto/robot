# third-party libraries
import mock
import pytest
from multiprocessing.managers import DictProxy, ListProxy, EventProxy
# project labraries
import settings
from robot.data import loader
from tests import mocks_data


@mock.patch('robot.data.loader.read_csv', return_value=mocks_data.PROXIES)
def test_load_proxies_loads_all_data(mock_read_csv):
    data = loader.read_csv(file='some CSV file')
    proxies_manager = loader.load_proxies(proxies=data)
    assert len(data) == len(proxies_manager), 'data and manager must have same lenght.'

@mock.patch('robot.data.loader.read_csv', return_value=mocks_data.PROXIES)
def test_load_proxies_returns_managers(mock_read_csv):
    data = loader.read_csv(file='some CSV file')
    manager = loader.load_proxies(proxies=data)
    assert isinstance(manager, ListProxy), 'Main list isn\'t a manager type.'
    for p in manager:
        assert isinstance(p, DictProxy), 'Nested dicts aren\'t manager type.'

@mock.patch('robot.data.loader.read_csv', return_value=mocks_data.PROXIES)
def test_load_proxies_response(mock_read_csv):
    data = loader.read_csv(file='some CSV file')
    manager = loader.load_proxies(proxies=data)
    keys = ['proxy', 'available', 'change_ip', 'counter', 'info', 'refresh']
    for p in manager:
        for k in keys:
            assert k in p, '%s not in response.' % k

@mock.patch('robot.data.loader.read_csv', return_value=mocks_data.PROXIES)
def test_load_proxies_event_types(mock_read_csv):
    data = loader.read_csv(file='some CSV file')
    manager = loader.load_proxies(proxies=data)
    keys = ['available', 'change_ip']
    for p in manager:
        for k in keys:
            assert isinstance(p.get(k), EventProxy), '%s isn\'t a Event type' % k
            assert not p.get(k).is_set(), '%s was loaded improperly.'

@mock.patch('robot.data.loader.read_csv', return_value=mocks_data.PROXIES)
def test_load_proxies_counter(mock_read_csv):
    data = loader.read_csv(file='some CSV file')
    manager = loader.load_proxies(proxies=data)
    for p in manager:
        assert isinstance(p.get('counter'), int), 'counter isn\'t a integer.'
        assert p.get('counter') == 0, 'counter need to be 0 during load.'

@mock.patch('robot.data.loader.read_csv', return_value=mocks_data.PROXIES)
def test_load_proxies_refresh(mock_read_csv):
    data = loader.read_csv(file='some CSV file')
    manager = loader.load_proxies(proxies=data)
    refresh = settings.PROXY_REFRESH_TIME
    for p in manager:
        assert isinstance(p.get('refresh'), int), 'refresh isn\'t a integer.'
        assert p.get('refresh') == refresh, 'counter need to be %s during load.' % refresh

@mock.patch('robot.data.loader.read_csv', return_value=mocks_data.CUSTOMERS)
def test_load_customers_loads_all_data(mock_read_csv):
    data = loader.read_csv(file='some CSV file')
    manager = loader.load_customers(customers=data)
    assert len(data) == len(manager), 'data and manager must have same lenght.'

@mock.patch('robot.data.loader.read_csv', return_value=mocks_data.CUSTOMERS)
def test_load_customers_returns_managers(mock_read_csv):
    data = loader.read_csv(file='some CSV file')
    manager = loader.load_proxies(proxies=data)
    assert isinstance(manager, ListProxy), 'Main list isn\'t a manager type.'
    for p in manager:
        assert isinstance(p, DictProxy), 'Nested dicts aren\'t manager type.'

@mock.patch('robot.data.loader.read_csv')
def test_load_customers_select_option(mock_read_csv):
    # De-select a customer
    mocks_data.CUSTOMERS[0]['select'] = ''
    mock_read_csv.return_value = mocks_data.CUSTOMERS
    data = loader.read_csv(file='some CSV file')
    manager = loader.load_customers(customers=data)
    assert len(data) != len(manager), 'data and manager couldn\'t have same lenght.'

@mock.patch('robot.data.loader.read_csv', return_value=mocks_data.PRODUCTS)
def test_load_products_returns_list_manager(mock_read_csv):
    products = loader.load_products()
    assert isinstance(products, ListProxy), 'load_products doen\'t return a list manager type.'

@mock.patch('robot.data.loader.read_csv', return_value=mocks_data.PRODUCTS)
def test_load_products_loads_only_selected(mock_read_csv):
    products = loader.load_products()
    selected_products = [p for p in mocks_data.PRODUCTS if p.get('select')]
    assert len(products) == len(selected_products), 'load_products loads not selected items.'

@mock.patch('robot.data.loader.read_csv', return_value=mocks_data.PRODUCTS)
def test_load_products_transforms_some_keys_in_list(mock_read_csv):
    products = loader.load_products()
    for p in products:
        for k in ['asins', 'weights', 'keywords']:
            if p.get(k):
                assert isinstance(p.get(k), list), 'load_products doen\'t transform %s key in list.' % k
        print(p)