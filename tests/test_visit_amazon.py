# Built-in modules
import mock
from multiprocessing import Manager
# Project modules
import settings
from robot.actions import visit_amazon


PROXY_INFO = {'status': 'success',
              'country':'United States',
              'countryCode':'US',
              'region':'GA',
              'regionName':'Georgia',
              'city':'Suwanee',
              'zip':'30024',
              'lat':34.0409,
              'lon':-84.0237,
              'timezone':'America/New_York',
              'isp':'Charter Communications',
              'org':'Spectrum',
              'as':'AS20115 Charter Communications',
              'query':'24.196.234.233'}

CUSTOMER = {'add_to_wish_list': '',
            'address_city': 'MARION',
            'address_country': 'US',
            'address_line1': '1665 25TH AVE',
            'address_line2': '',
            'address_state': 'MD',
            'address_zip': '52302-1127',
            'amazon_password': 'q4cOuHIn6',
            'email': 'paigethompson233@yahoo.com',
            'empty_cart': '',
            'languages': 'en_US',
            'mailbox_password': 'q4cOuHIn6',
            'name': 'Jillian Perry',
            'product_status': '',
            'purchase_the_first': '',
            'resolution': '1366,768',
            'select': 'x',
            'specific_asins': '',
            'user_agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                        '(KHTML, like Gecko) Chrome/58.0.3029.81',
            'imap_host': 'imap.mail.yahoo.com'}

def create_proxy_manager(proxy_host, proxy_info):
    """Creates a multiprocessing.Manager().dict()
    """
    m = Manager()
    m_proxy = m.dict({'proxy': proxy_host,
                      'available': m.Event(),
                      'change_ip': m.Event(),
                      'counter': 0,
                      'info': proxy_info,
                      'refresh': settings.PROXY_REFRESH_TIME})
    return m_proxy

def test_visit_amazon_process():
    """Tests if VisitAmazonProcess works properly."""
    # Mocks
    log_queue = mock.Mock()
    log_queue.info.return_value = None

    proxy = create_proxy_manager(proxy_host='163.172.111.11:1231',
                                 proxy_info=PROXY_INFO)
    customer = CUSTOMER

    va_process = visit_amazon.VisitAmazonProcess(proxy=proxy, customer=customer, log_queue=log_queue)
    assert va_process, 'VisitAmazonProcess not created properly.'
