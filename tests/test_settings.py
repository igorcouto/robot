# Built-in modules
import os
import warnings
# Project modules
import settings


def test_source_directory():
    src_dir = settings.SRC_DIR
    assert os.path.exists(src_dir), 'there isn\'t a source directory.'
    assert os.path.isdir(src_dir), '%s isn\'t a directory.' % src_dir

def test_log_directory():
    log_dir = settings.LOG_DIR
    assert os.path.exists(log_dir), 'there isn\'t a source directory.'
    assert os.path.isdir(log_dir), '%s isn\'t a directory.' % log_dir
    log_proxies = settings.PROXIES_LOG
    if os.path.exists(log_proxies):
        warnings.warn('There is %s log file. It will be appended.' % log_proxies, UserWarning)