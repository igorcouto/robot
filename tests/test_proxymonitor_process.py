# Built-in libraries
import time
from threading import Thread
from multiprocessing import Manager
# Third-party libraries
import mock
# Project libraries
from robot.proxymonitor import microleaves
from tests import mocks_proxymonitor as mock_pm


def create_proxy_manager(proxy_host):
    """Creates a multiprocessing.Manager().dict()
    """
    m = Manager()
    m_proxy = m.dict({'proxy': proxy_host,
                      'available': m.Event(),
                      'change_ip': m.Event(),
                      'counter': 0})
    return m_proxy

def test_ProxyMonitorProcess_threads():
    """Tests if ProxyMonitorProcess creates threads properly."""
    m_proxies = [create_proxy_manager(proxy_host='some PROXY HOST'),
                 create_proxy_manager(proxy_host='some PROXY HOST')]
    # Creates a process
    pm_process = microleaves.ProxyMonitorProcess(m_proxies=m_proxies)
    threads = pm_process.create_threads()
    for t in threads:
        assert isinstance(t, Thread), 'it isn\'t a Thread type.'

    assert len(m_proxies) == len(threads), 'Not all proxies are being monitored.'

def test_ProxyMonitorProcess_runs_until_terminate():
    """Tests if ProxyMonitorProcess runs forever."""
    m_proxies = [create_proxy_manager(proxy_host='some PROXY HOST'),
                 create_proxy_manager(proxy_host='some PROXY HOST')]
    # Creates a process
    pm = microleaves.ProxyMonitorProcess(m_proxies=m_proxies)
    pm.start()
    # Waits a little
    time.sleep(2)
    assert pm.is_alive(), 'ProxyMonitorProcess ends before being terminated.'
    pm.terminate()

def test_ProxyMonitorProcess_available_proxies():
    """Tests if available_proxies event in ProxyMonitorProcess."""
    m_proxies = [create_proxy_manager(proxy_host='163.172.111.11:1231'),
                 create_proxy_manager(proxy_host='163.172.111.11:1232')]
    # Creates a process
    pm = microleaves.ProxyMonitorProcess(m_proxies=m_proxies)
    assert pm.available_proxies, 'ProxyMonitorProcess doesn\'t have an available_proxies.'
    assert not pm.available_proxies.is_set(), 'ProxyMonitorProcess available_proxies not set by default.'

def test_ProxyMonitorProcess_available_proxies_behavior():
    """Tests if available_proxies event in ProxyMonitorProcess."""
    m_proxies = [create_proxy_manager(proxy_host='163.172.111.11:1231'),
                 create_proxy_manager(proxy_host='163.172.111.11:1232')]
    # Creates a process
    pm = microleaves.ProxyMonitorProcess(m_proxies=m_proxies)
    pm.start()
    # Waits for available_proxies event
    available_proxies = pm.available_proxies.wait(timeout=5)
    if len(pm.get_available_proxies()) > 0:
        assert available_proxies, 'available_proxies event not set even with available proxies.'
    pm.terminate()
    # Bad proxies
    m_proxies = [create_proxy_manager(proxy_host='8.8.8.8'),
                 create_proxy_manager(proxy_host='8.8.4.4')]
    # Creates a process
    pm = microleaves.ProxyMonitorProcess(m_proxies=m_proxies)
    pm.start()
    # Waits for available_proxies event
    assert not pm.available_proxies.is_set(), 'available_proxies event is set even with no available proxies.'
    pm.terminate()
