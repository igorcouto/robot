# Built-in module
import mock
from multiprocessing import Process, Queue
# Third-party module
import pytest
import psutil
# Project module
from robot.driver import chromedriver
from robot.procedure import interaction


def test_Interaction_is_Process():
    action = driver = mock.Mock()
    p = interaction.Interaction(driver=driver, action=action)
    assert isinstance(p, Process), 'Interaction isn\'t a Process type.'

def test_Interaction_no_args():
    action = driver = mock.Mock()
    p = interaction.Interaction(driver=driver, action=action)
    assert p, 'Interaction didn\'t initialize with no arguments.'

def test_Interaction_accepts_log_queue():
    action = driver = mock.Mock()
    log_queue = Queue()
    try:
        p = interaction.Interaction(driver=driver, action=action, log_queue=log_queue)
    except TypeError:
        pytest.fail('Interaction didn\'t accept log_queue argument.')

def test_Interaction_set_name():
    any_name = 'ANYTHING'
    action = driver = mock.Mock()
    p = interaction.Interaction(driver=driver, action=action, name=any_name)
    assert p._name == any_name, 'process name not configured.'

def test_Interaction_opens_browser_on_start():
    func = mock.Mock()
    func.__reduce__ = lambda self: (mock.MagicMock, ())
    action = {'func': func}
    driver = chromedriver.ChromeDriver()
    p = interaction.Interaction(driver=driver, action=action)
    with mock.patch.object(chromedriver.ChromeDriver, 'initialize') as mock_driver:
        p.start()
        assert driver.is_available.wait(timeout=5)
        p.join()

def test_Interaction_closes_browser_on_terminate():
    func = mock.Mock()
    func.__reduce__ = lambda self: (mock.MagicMock, ())
    action = {'func': func}
    driver = chromedriver.ChromeDriver()
    p = interaction.Interaction(driver=driver, action=action)
    p.start()
    p.driver_is_available.wait()
    pid = p.driver_pid.value # driver pid
    p.terminate()
    assert not psutil.pid_exists(pid), 'driver remains opened even thought terminate called.'

@mock.patch('psutil.Process.kill')
def test_Interaction_terminate_handlers_exceptions(mock_psutil_kill):
    mock_psutil_kill.side_effect = PermissionError
    func = mock.Mock()
    func.__reduce__ = lambda self: (mock.MagicMock, ())
    action = {'func': func}
    driver = chromedriver.ChromeDriver()
    p = interaction.Interaction(driver=driver, action=action)
    p.start()
    pid = p.driver_pid.value # driver pid
    p.terminate()