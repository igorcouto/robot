# Buit-in modules
from multiprocessing import Manager
# Project modules
import settings
from robot.utils import combine
import tests.mocks_combine as mock_comb


def create_proxy_manager(proxy_host, proxy_info):
    """Creates a multiprocessing.Manager().dict()
    """
    m = Manager()
    m_proxy = m.dict({'proxy': proxy_host,
                      'available': m.Event(),
                      'change_ip': m.Event(),
                      'counter': 0,
                      'info': proxy_info,
                      'refresh': settings.PROXY_REFRESH_TIME})
    return m_proxy

def test_combine_proxies_and_customers():
    """Tests if combine_proxies_and_customer works properly."""
    proxies = [create_proxy_manager(proxy_host='163.172.111.11:1231',
                                    proxy_info=mock_comb.PROXY_INFO_01),
               create_proxy_manager(proxy_host='163.172.111.11:1232',
                                    proxy_info=mock_comb.PROXY_INFO_02),
               create_proxy_manager(proxy_host='163.172.111.11:1233',
                                    proxy_info=mock_comb.PROXY_INFO_03)]
    customers = mock_comb.CUSTOMERS
    # Test combine
    proxy, customer = combine.combine_proxies_and_customers(proxies=proxies,
                                                            customers=customers)
    assert proxy, 'no proxy is selected.'
    assert customer, 'no customer is selected.'