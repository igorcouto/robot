# Built-in modules
import os
import warnings
# Project modules
import settings


def test_userdata_tmp_dir():
    tmp_dir = settings.USERDATA_TEMP
    assert os.path.exists(tmp_dir), 'there isn\'t a tpm directory for userdata.'
    assert os.path.isdir(tmp_dir), '%s isn\'t a directory.' % tmp_dir

def test_userdata_zipped_dir():
    zipped_dir = settings.USERDATA_ZIPPED
    assert os.path.exists(zipped_dir), 'there isn\'t a directory to store zipped userdata.'
    assert os.path.isdir(zipped_dir), '%s isn\'t a directory.' % zipped_dir