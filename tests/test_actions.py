# Built-in modules
import mock
# Third-part modules
import pytest
from selenium.common.exceptions import TimeoutException, NoSuchElementException
# Project modules
from robot.driver import chromedriver
from robot.actions import common as common_actions


def test_go_to_initial_page_handlers_TimeoutException():
    driver =  mock.MagicMock()
    driver.is_available.is_set.return_value = True
    driver.get.side_effect = TimeoutException
    try:
        common_actions.go_to_initial_page(driver)
    except TimeoutException:
        pytest.fail('TimeoutException not handled inside method.')

def test_clicks_in_nav_logo_handlers_TimeoutException():
    driver =  mock.MagicMock()
    driver.is_available.is_set.return_value = True
    driver.find_element_by_id.side_effect = TimeoutException
    try:
        common_actions.clicks_in_nav_logo(driver)
    except TimeoutException:
        pytest.fail('TimeoutException not handled inside method.')

def test_clicks_in_nav_logo_handlers_NoSuchElementException():
    driver =  mock.MagicMock()
    driver.is_available.is_set.return_value = True
    driver.find_element_by_id.side_effect = NoSuchElementException
    try:
        common_actions.clicks_in_nav_logo(driver)
    except NoSuchElementException:
        pytest.fail('NoSuchElementException not handled inside method.')

def test_type_on_search_bar_handlers_TimeoutException():
    word = 'home'
    driver =  mock.MagicMock()
    driver.is_available.is_set.return_value = True
    driver.find_element_by_id.side_effect = TimeoutException
    try:
        common_actions.type_on_search_bar(driver, word=word)
    except TimeoutException:
        pytest.fail('TimeoutException not handled inside method.')

def test_type_on_search_bar_handlers_NoSuchElementException():
    word = 'home'
    driver =  mock.MagicMock()
    driver.is_available.is_set.return_value = True
    driver.find_element_by_id.side_effect = NoSuchElementException
    try:
        common_actions.type_on_search_bar(driver, word=word)
    except NoSuchElementException:
        pytest.fail('TimeoutException not handled inside method.')

